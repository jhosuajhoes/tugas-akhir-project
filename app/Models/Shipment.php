<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    public $timestamps = false;
    use HasFactory;
    protected $guarded=['id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
