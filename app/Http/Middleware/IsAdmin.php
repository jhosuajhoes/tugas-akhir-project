<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!auth()->check() || auth()->user()->roles !== 'admin' && auth()->user()->roles !== 'owner'){
            Alert::error('unauthorized', 'You not authorized');
            return redirect()->back();
        }
        return $next($request);
    }
}
