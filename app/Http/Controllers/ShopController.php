<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    public function show(){
        /* cek login */
        if (Auth::check()) {
            $wish_count = Cart::where('user_id',Auth::user()->id)->where('status','=','Wishlist')->count();
            $wishlist = Cart::where('user_id',Auth::user()->id)->where('status','=','Wishlist')->get();
            $count_order = Order::whereIn('unique_code',function($query){
                                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
                            })->count();
            $count = Cart::where('user_id',Auth::user()->id)->where('status','=','Cart')->count();
            $checkouts = Cart::where('user_id',Auth::user()->id)->where('status','=','Cart')->get();
            $categories = Category::all();

            /* kondisi filter product */
            $price = request('price-range');
            $cat_filter = request('category-filter');

            /* filter berdasarkan range harga */
            if ($price != null && $cat_filter==null) {
                if ($price == '50000') {
                    $category = Category::where('name',$cat_filter)->first();
                    $search =  Product::where('price','<',$price)
                    ->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::where('price','<',$price)->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
                } elseif($price == '50000-75000') {
                    $category = Category::where('name',$cat_filter)->first();
                    $search = Product::whereBetween('price',['50000','75000'])->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::whereBetween('price',['50000','75000'])->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
                }
                elseif($price == '75000-120000') {
                    $category = Category::where('name',$cat_filter)->first();
                    $search = Product::whereBetween('price',['75000','120000'])->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::whereBetween('price',['75000','120000'])->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
                }
                else {
                    $category = Category::where('name',$cat_filter)->first();
                    $search =  Product::where('price','>',$price)
                    ->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::where('price','>',$price)->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
                }
            }
            /* filter berdasarkan kategori */
            elseif($price == 0 && $cat_filter) {
                    $category = Category::where('name',$cat_filter)->first();
                    $search = Product::where('category_id',$category->id)->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::where('category_id',$category->id)->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
            }
            /* filter range harga 50000 dengan kategori */
            elseif($price == '50000' && $cat_filter){
                $category = Category::where('name',$cat_filter)->first();
                $search = Product::where('price','<',$price)
                ->where('category_id',$category->id)->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::where('price','<',$price)
                                            ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
            }
            /* filter range harga 50000-75000 dengan kategori */
            elseif($price == '50000-75000' && $cat_filter){
                $category = Category::where('name',$cat_filter)->first();
                $search = Product::whereBetween('price',['50000','75000'])->where('category_id',$category->id)->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::whereBetween('price',['50000','75000'])
                                            ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
            }
            /* filter range harga 75000-120000 dengan kategori */
            elseif($price == '75000-120000' && $cat_filter){
                $category = Category::where('name',$cat_filter)->first();
                $search = Product::whereBetween('price',['75000','120000'])->where('category_id',$category->id)->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::whereBetween('price',['75000','120000'])
                                            ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
            }
            /* filter range harga 120000 dengan kategori */
            elseif($price == '120000' && $cat_filter){
                $category = Category::where('name',$cat_filter)->first();
                $search = Product::where('price','>',$price)
                ->where('category_id',$category->id)->groupBy('name')->get();
                    return view('customer.shop',[
                        'active' => 'shop',
                        'wish_count' =>$wish_count,
                        'wishlist' =>$wishlist,
                        'count_order' =>$count_order,
                        'count' =>$count,
                        'checkouts' =>$checkouts,
                        'categories' =>$categories,
                        'products' => Product::where('price','>',$price)
                                            ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                        'search_results' =>count($search),
                    ]);
            }
            /* kondisi fitur searching */
            elseif (request('search')) {
                $search = Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->get();
                return view('customer.shop',[
                    'active' => 'shop',
                    'wish_count' =>$wish_count,
                    'wishlist' =>$wishlist,
                    'count_order' =>$count_order,
                    'count' =>$count,
                    'checkouts' =>$checkouts,
                    'categories' =>$categories,
                    'products' => Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->paginate(6),
                    'search_results' =>count($search),
                ]);
            }
            else{
                return view('customer.shop',[
                    'active' => 'shop',
                    'wish_count' =>$wish_count,
                    'wishlist' =>$wishlist,
                    'count_order' =>$count_order,
                    'count' =>$count,
                    'checkouts' =>$checkouts,
                    'categories' =>$categories,
                    'products' => Product::groupBy('name')->orderBy('price','ASC')->paginate(6),
                    'search_results' => 'Product',
                ]);
            }
        }

    /* belum login */

        /* kondisi filter product */
        $price = request('price-range');
        $cat_filter = request('category-filter');
        $categories = Category::all();

        /* filter berdasarkan range harga */
        if ($price != null && $cat_filter==null) {
            if ($price == '50000') {
                $search = Product::where('price','<',$price)->groupBy('name')->get();
                $category = Category::where('name',$cat_filter)->first();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::where('price','<',$price)->groupBy('name')->paginate(6),
                    'search_results' => count($search),
                ]);
            } elseif($price == '50000-75000') {
                $search = Product::whereBetween('price',['50000','75000'])->groupBy('name')->get();
                $category = Category::where('name',$cat_filter)->first();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::whereBetween('price',['50000','75000'])->groupBy('name')->paginate(6),
                    'search_results' => count($search),
                ]);
            }
            elseif($price == '75000-120000') {
                $search =  Product::whereBetween('price',['75000','120000'])->groupBy('name')->get();
                $category = Category::where('name',$cat_filter)->first();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::whereBetween('price',['75000','120000'])->groupBy('name')->paginate(6),
                    'search_results' =>count($search),
                ]);
            }
            elseif($price == '120000') {
                $search =  Product::where('price','>=',$price)->groupBy('name')->get();
                $category = Category::where('name',$cat_filter)->first();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::where('price','>=',$price)->groupBy('name')->paginate(6),
                    'search_results' =>count($search),
                ]);
            }
            else {
                $category = Category::where('name',$cat_filter)->first();
                $search = Product::where('price','>',$price)->groupBy('name')->get();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::where('price','>',$price)->groupBy('name')->paginate(6),
                    'search_results' => count($search),
                ]);
            }
        }
        /* filter berdasarkan kategori */
        elseif($price == 0 && $cat_filter) {
            $category = Category::where('name',$cat_filter)->first();
            $search = Product::where('category_id',$category->id)->groupBy('name')->get();
            return view('customer.shop',[
                'active' => 'shop',
                'categories' =>$categories,
                'products' => Product::where('category_id',$category->id)->groupBy('name')->paginate(6),
                'search_results' =>count($search),
            ]);
        }
        /* filter range harga 50000 dengan kategori */
        elseif($price == '50000' && $cat_filter){
            $category = Category::where('name',$cat_filter)->first();
            $search = Product::where('price','<',$price)->where('category_id',$category->id)->groupBy('name')->get();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::where('price','<',$price)
                                        ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                'search_results' =>count($search),
                ]);
        }
        /* filter range harga 50000-75000 dengan kategori */
        elseif($price == '50000-75000' && $cat_filter){
            $category = Category::where('name',$cat_filter)->first();
            $search = Product::whereBetween('price',['50000','75000'])->where('category_id',$category->id)->groupBy('name')->get();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::whereBetween('price',['50000','75000'])
                                        ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                    'search_results' =>count($search),
                ]);
        }
        /* filter range harga 75000-120000 dengan kategori */
        elseif($price == '75000-120000' && $cat_filter){
            $category = Category::where('name',$cat_filter)->first();
            $search = Product::whereBetween('price',['75000','120000'])->where('category_id',$category->id)->groupBy('name')->get();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::whereBetween('price',['75000','120000'])
                                        ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                    'search_results' =>count($search),
                ]);
        }
        /* filter range harga 120000 dengan kategori */
        elseif($price == '120000' && $cat_filter){
            $category = Category::where('name',$cat_filter)->first();
            $search = Product::where('price','>',$price)
            ->where('category_id',$category->id)->groupBy('name')->get();
                return view('customer.shop',[
                    'active' => 'shop',
                    'categories' =>$categories,
                    'products' => Product::where('price','>',$price)
                                        ->where('category_id',$category->id)->groupBy('name')->paginate(6),
                    'search_results' =>count($search),
                ]);
        }
        /* kondisi fitur searching */
        elseif (request('search')) {
            $search = Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->get();
            return view('customer.shop',[
                'active' => 'shop',
                'products' => Product::where('name', 'like', '%' . request('search') . '%' )->groupBy('name')->paginate(6),
                'search_results' =>count($search),
                'categories' => Category::all()
            ]);
        }
        else {
            return view('customer.shop',[
                'active' => 'shop',
                'products' => Product::groupBy('name')->paginate(6),
                'search_results' => 'Product',
                'categories' => Category::all()
            ]);
        }
    }

    public function category_filter($category){
        if (Auth::check()) {
            $categoryID = Category::where('name',$category)->first();
            if (Product::where('category_id',$categoryID->id)->exists()) {
                $show = Product::where('category_id',$categoryID->id)
                                ->groupBy('name')->paginate(6);
                return view('customer.category',[
                    'active' => 'category',
                    'wish_count' => Cart::where('user_id',Auth::user()->id)
                    ->where('status','=','Wishlist')->count(),
                    'wishlist' => Cart::where('user_id',Auth::user()->id)
                    ->where('status','=','Wishlist')->get(),
                    'count_order' => Order::whereIn('unique_code',function($query){
                        $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
                    })->count(),
                    'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
                    'products' => $show,
                    'search_results' => 'Product',
                    'categories' => Category::all(),
                    'checkouts' => Cart::where('user_id',Auth::user()->id)
                                        ->where('status','=','Cart')->get()
                ]);
                } else {
                return redirect('/shop')->with('toast_info','Product not available');
                }
        } else {
            $categoryID = Category::where('name',$category)->first();
            if (Product::where('category_id',$categoryID->id)->exists()) {
                $categoryID = Category::where('name',$category)->first();
                $show = Product::where('category_id',$categoryID->id)
                                ->groupBy('name')->paginate(6);
                return view('customer.category',[
                    'active' => 'category',
                    'products' => $show,
                    'search_results' => 'Product',
                    'categories' => Category::all(),
                ]);
                } else {
                return redirect('/shop')->with('toast_info','Product not available');
                }
        }
    }

}
