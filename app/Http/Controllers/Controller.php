<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use App\Models\Category;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        // $count = Cart::select('product_id', DB::raw('COUNT(product_id) AS product'))->where('status','Ordered')->groupBy('product_id')->orderBy('product', 'ASC')->limit(2)->get();
        // foreach ($count as $key => $value) {
        //     $product_best = Product::where('id',$value->product_id)->get();
        // }
        if (Auth::check()) {
            return view('index',[
                'active' => 'home',
                'categories' => Category::all(),
                'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
                'count_order' => Order::whereIn('unique_code',function($query){
                                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
                            })->count(),
                'checkouts' => Cart::where('user_id',Auth::user()->id)
                                                ->where('status','=','Cart')->get(),
                'wish_count' => Cart::where('user_id',Auth::user()->id)
                ->where('status','=','Wishlist')->count(),
                'wishlist' => Cart::where('user_id',Auth::user()->id)
                ->where('status','=','Wishlist')->get(),
                'products' => Cart::where('status','Ordered')->orderBy('quantity','DESC')->groupBy('product_id')->take(3)->get()
            ]);
        }else{
            return view('index',[
                'active' => 'home',
                'products' => Cart::where('status','Ordered')->orderBy('quantity','desc')->take(3)->get(),
                'categories' => Category::all()
            ]);
        }
    }

    public function about()
    {
        if (Auth::check()) {
            return view('customer.about',[
                'active' => 'about',
                'categories' => Category::all(),
                'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
                'count_order' => Order::whereIn('unique_code',function($query){
                                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
                            })->count(),
                'checkouts' => Cart::where('user_id',Auth::user()->id)
                                                ->where('status','=','Cart')->get(),
                'wish_count' => Cart::where('user_id',Auth::user()->id)
                ->where('status','=','Wishlist')->count(),
                'wishlist' => Cart::where('user_id',Auth::user()->id)
                ->where('status','=','Wishlist')->get(),
            ]);
        }else{
            return view('customer.about',[
                'active' => 'about',
                'categories' => Category::all()
            ]);
        }
    }
}
