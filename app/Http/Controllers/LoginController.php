<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\VerifyUser;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Validation\Rules\Password;

class LoginController extends Controller
{
    public function index()
    {
        return view('customer.login');
    }

    public function verify(Request $request){
        $token = $request->token;
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(!is_null($verifyUser)){
            $user = $verifyUser->user;

            if(!$user->email_verified){
                $verifyUser->user->email_verified = 'verified';
                $verifyUser->user->save();

                return redirect()->route('login')->with('success','Your email is verified successfully. You can now login')->with('verifiedEmail', $user->email);
            }else{
                 return redirect()->route('login')->with('info','Your email is already verified. You can now login')->with('verifiedEmail', $user->email);
            }
        }
    }

    public function authenticate(Request $request)
    {
        $credentials= $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        $email = $request->email;
        $is_verified = User::where('email',$email)->where('email_verified','verified')->count();
        $cek_email = User::where('email',$email)->count();
        $redirect_admin = User::where('email',$email)->where('roles','admin')->count();
        $redirect_owner = User::where('email',$email)->where('roles','owner')->count();
        if ($is_verified > 0) {
            if ($redirect_admin) {
                if (Auth::attempt($credentials)) {
                    $request->session()->regenerate();
                    return redirect()->intended('/admin/dashboard');
                }else{
                    Alert::warning('Failed', 'Your email and password combination does not match');
                    return back();
                }
            }elseif ($redirect_owner) {
                if (Auth::attempt($credentials)) {
                    $request->session()->regenerate();
                    return redirect()->intended('/admin/dashboard');
                }else{
                    Alert::warning('Failed', 'Your email and password combination does not match');
                    return back();
                }
            }
            else{
                if (Auth::attempt($credentials)) {
                    $request->session()->regenerate();
                    return redirect()->intended('/');
                }else{
                    Alert::warning('Failed', 'Your email and password combination does not match');
                    return back();
                }
            }
        }elseif ($cek_email == 0) {
            Alert::warning('Failed', 'Email is not registered, please register first');
            return back();
        }
        else{
            Alert::warning('Failed', 'Please verified your email first');
            return back();
        }
    }
    public function logout()
    {
        request()->session()->flush();
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }

    public function forgotPassword()
    {
        return view('customer.forgot-password');
    }

    public function sendResetLink(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);

        $token = Str::random(64);
        DB::table('password_resets')->insert([
              'email'=>$request->email,
              'token'=>$token,
              'created_at'=>Carbon::now(),
        ]);
        $action_link = route('reset.password.form',['token'=>$token,'email'=>$request->email]);
        $body = "We have received a request to reset the password for <b>Skysea.co</b> account associated with ".$request->email.". You can reset your password by click link below.";

        Mail::send('customer.email-forgot',['action_link'=>$action_link,'body'=>$body], function($message) use ($request){
             $message->from('noreply@example.com', 'Skysea.co');
             $message->to($request->email, 'Username')
                     ->subject('Reset Password');
        });
        Alert::success('Success', 'We have e-mailed your password reset link, please check in ' . $request->email . ' inbox or spam');
        return back();
    }

    public function showResetForm(Request $request, $token = null){
        return view('customer.reset-password')->with(['token'=>$token,'email'=>$request->email]);
    }

    public function resetPassword(Request $request){
        $request->validate([
             'email'=>'required|email|exists:users,email',
             'password'=>['required',Password::min(8)->mixedCase()->letters()->numbers(),],
        ]);

        $check_token = DB::table('password_resets')->where([
             'email'=>$request->email,
             'token'=>$request->token,
        ])->first();

        if(!$check_token){
            return back()->withInput()->with('fail', 'Invalid token');
        }else{
            User::where('email', $request->email)->update([
                'password'=>bcrypt($request->password)
            ]);

            DB::table('password_resets')->where([
                'email'=>$request->email
            ])->delete();

            Alert::success('Success', 'Your password has been changed! You can login with new password');
            return redirect('/login');
        }
    }
}
