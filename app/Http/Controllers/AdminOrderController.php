<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class AdminOrderController extends Controller
{
    public function index()
    {
        $order = Order::all();
        return view('admin.order.index',[
            'active' => 'order',
            'orders' => $order
        ]);
    }

    public function order_details($unique_code)
    {
        return view('admin.order.order-details',[
            'active' => 'order',
            'order' => Order::where('unique_code',$unique_code)->first(),
            'order_cart' => Cart::where('unique_code',$unique_code)->where('status','!=','Cart')->get()
        ]);
    }

    public function order_invoice($unique_code)
    {
        return view('admin.order.order-invoice',[
            'order' => Order::where('unique_code',$unique_code)->first(),
            'order_cart' => Cart::where('unique_code',$unique_code)->where('status','!=','Cart')->get()
        ]);
    }

    public function update_status(Request $request, $unique_code)
    {
        if ($request->qty != null) {
            /* update stock product */
            $stock = $request->stock;
            $qty = $request->qty;
            $a = count($qty);
            $id = $request->product_id;
            for ($i=0; $i < $a ; $i++) {
                $new_stock =$stock[$i] + $qty[$i];
                $validatedProduct['stock'] = $new_stock;
                $product_id = $id[$i];
                Product::where('id',$product_id)->update($validatedProduct);
            }
        }
        $validatedData['status'] = $request->status;
        Order::where('unique_code',$unique_code)->update($validatedData);
        return redirect()->back()->with('toast_success', 'Order status updated!');
    }
}
