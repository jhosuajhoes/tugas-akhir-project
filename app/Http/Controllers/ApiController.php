<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function payment_handler(Request $request)
    {
        $json = json_decode($request->getContent());
        $signature_key = hash('sha512',$json->order_id . $json->status_code . $json->gross_amount . env('MIDTRANS_SERVER_KEY'));
        if ($signature_key == $json->signature_key) {
            $order = Order::where('unique_code', $json->order_id)->first();
            /* return stok jika transaksi gagal */
            if ($json->transaction_status == 'failure') {
                $cart = Cart::where('unique_code',$json->order_id)->get();
                foreach ($cart as $value) {
                    $quantity[]=$value->quantity;
                    $id_product[]=$value->product->get(0)->id;
                    $stock_product[]=$value->product->get(0)->stock;
                }
                $qty = $quantity;
                $stock = $stock_product;
                $id = $id_product;
                $a = count($qty);
                for ($i=0; $i < $a ; $i++) {
                    $new_stock =$stock[$i] + $qty[$i];
                    $validatedProduct['stock'] = $new_stock;
                    $product_id = $id[$i];
                    Product::where('id',$product_id)->update($validatedProduct);
                }
                $validatedCart['status'] = 'Failure';
                Cart::where('unique_code',$json->order_id)->update($validatedCart);
                return $order->update(['status'=>$json->transaction_status]);
            } else {
                return $order->update(['status'=>$json->transaction_status]);
            }
        }else{
            return 'Invalid Order';
        }
    }
}
