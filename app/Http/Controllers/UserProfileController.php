<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\User;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Category;
use App\Models\Shipment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class UserProfileController extends Controller
{
    public function index()
    {
        return view('customer.profile',[
            'active' => 'profile',
            'categories' => Category::all(),
            'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                            ->where('status','=','Cart')->get(),
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'count_order' => Order::whereIn('unique_code',function($query){
                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
            })->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get()
        ]);
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        $validatedData = $request->validate([
            'name' => 'required|min:4|max:255',
            'phone_number' => 'required|min:10|max:14',
            'address' => 'required|min:5|max:255',
        ]);

        User::where('id',$id)->update($validatedData);
        return redirect('/profile')->with('success','Your Profile has been Update');
    }

    public function destroy($id)
    {
        Payment::where('user_id',$id)->delete();
        Shipment::where('user_id',$id)->delete();
        $code = Cart::where('user_id',$id)->count();
        $unique_code = Cart::where('user_id',$id)->get();
        for ($i=0; $i < $code ; $i++) {
            $uuc = $unique_code->get($i)->unique_code;
            Order::where('unique_code',$uuc)->delete();
        }
        Cart::where('user_id',$id)->delete();
        User::destroy($id);
        return redirect('/login')->with('toast_error', 'Your Account has been Deleted, please login or register with new account!');
    }

    public function security()
    {
        return view('customer.profile-security',[
            'active' => 'security',
            'categories' => Category::all(),
            'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                            ->where('status','=','Cart')->get(),
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'count_order' => Order::whereIn('unique_code',function($query){
                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
            })->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get()
        ]);
    }

    public function change_password(Request $request)
    {
        if (Hash::check($request->old_password, Auth::user()->password)) {
            $validatedData= $request->validate([
                'password' => 'required|min:5|max:255'
            ]);

            $validatedData['password'] = bcrypt($validatedData['password']);
            User::where('id',Auth::user()->id)->update($validatedData);
            Alert::success('Success', 'Password Successfully Change');
            return redirect()->back();
        }
        Alert::warning('Password lama salah', 'Mohon periksa kembali password lama anda');
        return redirect()->back();
}
}
