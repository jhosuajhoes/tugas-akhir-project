<?php

namespace App\Http\Controllers;

use Midtrans\Snap;
use App\Models\Cart;
use App\Models\City;
use App\Models\User;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Category;
use App\Models\Shipment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PaymentController extends Controller
{
    public function show(Request $request)
    {
        /* midtrans code start */

        // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = env('MIDTRANS_SERVER_KEY');
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

            $params = array(
                'transaction_details' => array(
                    'order_id' => $request->order_id,
                    'gross_amount' => $request->total_bayar,
                ),
                'customer_details' => array(
                    'first_name' => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone_number,
                ),
            );
            $unique_code = $request->order_id;

        $snapToken = Snap::getSnapToken($params);

        $city = City::where('id',$request->destination)->first();
        /* midtrans code end */

        $est = $request->estimasi;
        $date = date("Y-m-d"); /* pendefinisian tanggal awal */
        $est_date = date('Y-m-d', strtotime("+$est days", strtotime($date)));

        return view('customer.payment',[
            'active' => 'shop',
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get(),
            'count_order' => Order::whereIn('unique_code',function($query){
                $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
            })->count(),

            'categories' => Category::all(),
            'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
            'users' => User::all(),
            'checkouts' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Cart')->get(),
            'snap_token' => $snapToken,
            'unique_code' => $unique_code,
            'data_request' => $request,
            'estimasi' => $est_date,
            'city' => $city
        ]);

    }

    public function payment(Request $request)
    {
        /* decode request json to array */
        $json = json_decode($request->json);
        $expire = isset($json->result->transaction_status) ? $json->result->transaction_status:null;
        if ($expire == 'expire') {
            Alert::error('Payment Info', 'Waktu pembayaran anda telah habis, silahkan melakukan pembayaran ulang');
            return redirect('/checkout/shipment');
        }

        $validatedOrder['unique_code'] =$json->order_id;
        $validatedOrder['gross_amount'] =$json->gross_amount;
        $validatedOrder['transaction_id'] =$json->transaction_id;
        $validatedOrder['date'] =$json->transaction_time;

        $validatedOrder['status'] = $json->transaction_status;
        if ($json->transaction_status == 'capture') {
            $validatedOrder['status'] ='settlement';
        }
        /* update stock product */
        $stock = $request->stock;
        $qty = $request->qty;
        $a = count($qty);
        $id = $request->product_id;
        for ($i=0; $i < $a ; $i++) {
            $new_stock =$stock[$i] - $qty[$i];
            $validatedProduct['stock'] = $new_stock;
            $product_id = $id[$i];
            Product::where('id',$product_id)->update($validatedProduct);
        }

        /* set status cart to order or ordered berdasarkan status pembayaran */
        $payment_status = $json->transaction_status;
        $code_id = $json->order_id;
        if ($payment_status == 'pending') {
            $validatedCart['status'] = 'Pending';
        } else {
            $validatedCart['status'] = 'Ordered';
        }
        Cart::where('unique_code',$code_id)->update($validatedCart);
        Order::create($validatedOrder);

        /* mengambil unique code untuk di input kedalam table shipment */
        $unique_code =$json->order_id;
        $order_id_shipment = Order::where('unique_code',$unique_code)->get();

        /* input data shipment */
        $est = $request->estimation;
        $date = $json->transaction_time;
        $est_date = date('Y-m-d', strtotime("+$est days", strtotime($date)));

        $validatedShipment['arrival_date'] = $est_date;
        $validatedShipment['courier'] = $request->courier;

        $validatedShipment['shipping_cost'] = $request->shipment_cost;
        $validatedShipment['address'] = $request->address;
        $validatedShipment['city_id'] = $request->city;
        $validatedShipment['order_id'] = $order_id_shipment->get(0)->id;
        $validatedShipment['user_id'] = Auth::user()->id;
        Shipment::create($validatedShipment);

        /* input data payment */
        $validatedPayment['order_id'] = $order_id_shipment->get(0)->id;
        $validatedPayment['user_id'] = Auth::user()->id;
        $validatedPayment['gross_amount'] =$json->gross_amount;
        $validatedPayment['store'] =isset($json->store) ? $json->store:null;
        $validatedPayment['payment_code'] =isset($json->payment_code) ? $json->payment_code:null;
        $validatedPayment['va_number'] =isset($json->va_numbers[0]->va_number) ? $json->va_numbers[0]->va_number : null;
        if ($json->payment_type == 'credit_card') {
            $validatedPayment['bank'] =$json->bank;
            $validatedPayment['card_type'] =$json->card_type;
        }else{
            $validatedPayment['bank'] =isset($json->va_numbers[0]->bank) ? $json->va_numbers[0]->bank : null;
        }
        $validatedPayment['gross_amount'] =$json->gross_amount;
        $validatedPayment['payment_type'] =$json->payment_type;
        $validatedPayment['invoice'] =isset($json->pdf_url) ? $json->pdf_url : null;
        Payment::create($validatedPayment);

        /* kondisi untuk menampilkan alert */
        $status = $json->transaction_status;
        if ($status == 'settlement') {
            Alert::success('Payment Success', 'Pembayaran berhasil dilakukan silahkan cek pada halaman order');
            return redirect('/order');
        }elseif ($status == 'capture') {
            Alert::success('Payment Success', 'Pembayaran berhasil dilakukan silahkan cek pada halaman order');
            return redirect('/order');
        }elseif ($status == 'pending') {
            if($json->payment_type == 'qris'){
                $orderr = Order::where('unique_code',$json->order_id)->first();
                Payment::where('order_id',$orderr->id)->delete();
                Shipment::where('order_id',$orderr->id)->delete();
                Order::where('unique_code',$json->order_id)->delete();
                $updateCart['status'] = 'Cart';
                $updateCart['unique_code'] = Str::random(6);
                Cart::where('unique_code',$json->order_id)->update($updateCart);
                Alert::warning('Payment Info', 'Anda belum melakukan scan QR code yang diberikan, silahkan melakukan pembayaran ulang');
                return redirect('/checkout/shipment');
            }
            Alert::info('Payment Info', 'Mohon segera melakukan pembayaran sebelum tenggat waktu yang ditentukan');
            return redirect('/order');
        } else {
            Alert::error('Payment Error', 'Terjadi kesalahan dalam pembayaran');
            // $reset_code = Str::random(6);
            // $data['unique_code'] = $reset_code;
            // Cart::where('unique_code',$json->order_id)->update($data);
            return redirect('/checkout');
        }
    }
}
