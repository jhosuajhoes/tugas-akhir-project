<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use RealRashid\SweetAlert\Facades\Alert;
use function PHPUnit\Framework\returnValueMap;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.wishlist',[
            'active' => 'shop',
            'wish_count' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->count(),
            'wishlist' => Cart::where('user_id',Auth::user()->id)
            ->where('status','=','Wishlist')->get(),
            'categories' => Category::all(),
            'count' => Cart::where('user_id',Auth::user()->id)
                        ->where('status','=','Cart')->count(),
            'count_order' => Order::whereIn('unique_code',function($query){
                            $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
                        })->count(),

            'checkouts' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Cart')->get(),

            'wishlists' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Wishlist')->get(),
            'old_code' => Cart::where('user_id',Auth::user()->id)
                                ->where('status','=','Cart')->first(),
            'code' => Str::random(6),
            'counting' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Wishlist')->count(),
            'qty_sum' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','Wishlist')->sum('quantity'),
            'total_harga' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','Wishlist')
                            ->sum('price_total'),
            'product' =>Product::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $qty = $request->quantity;
        $user_id = $request->user_id;
        $code_id = $request->old_code;
        $product_id = $request->product_id;
        $unique_code = $request->unique_code;
        $price_total = $request->price_total;
        $cek_stock = Product::where('id',$product_id)->first();
        $stok = $cek_stock->stock;
        $cek = Cart::where('user_id',Auth::user()->id)
                ->where('status','Cart')->count();
        if ($qty <= $stok) {
            /* cek keranjang */
            if ($cek > 0) {
                $cekbrg = Cart::where('user_id',Auth::user()->id)
                        ->where('product_id',$product_id)
                        ->where('unique_code',$code_id)->count();

                $qty = request('quantity');
                $old_qty = request('qty_old');

                /* cek barang serupa */
                if ($cekbrg>0) {
                    $old_qty = Cart::where('product_id',$product_id)
                                    ->where('unique_code',$code_id)->first();
                    $quantity_old = $old_qty->quantity;

                    $new_qty = $qty + $quantity_old;
                    $prc_product = $old_qty->product->get(0)->price;
                    $total_harga = $new_qty*$prc_product;

                    $validatedData = $request->validate([
                        'date' => 'required',
                    ]);
                    $validatedData['quantity'] = $new_qty;
                    $validatedData['price_total'] = $total_harga;

                    Cart::where('product_id', $product_id)
                        ->where('unique_code',$code_id)->update($validatedData);
                    /* hapus data di wishlist */
                    Cart::where('product_id', $product_id)
                        ->where('user_id',$user_id)
                        ->where('status','Wishlist')->delete();
                    return redirect('/checkout')->with('toast_success', 'Berhasil update keranjang!');;
                } else {
                /* barang beda dengan kode order sama */
                    $validatedData = $request->validate([
                        'quantity' => 'required',
                        'user_id' => 'required',
                        'date' => 'required',
                    ]);
                    $validatedData['status'] = 'Cart';
                    $validatedData['product_id'] = $product_id;
                    $validatedData['unique_code'] = $code_id;
                    $validatedData['price_total'] = $price_total;
                    Cart::create($validatedData);
                    /* hapus data di wishlist */
                    Cart::where('product_id', $product_id)
                        ->where('user_id',$user_id)
                        ->where('status','Wishlist')->delete();
                    return redirect('/checkout')->with('toast_success', 'Berhasil input keranjang!');
                }
            } else {
                /* belum pernah order */
                $validatedData = $request->validate([
                    'quantity' => 'required',
                    'user_id' => 'required',
                    'date' => 'required',
                ]);

                $validatedData['status'] = 'Cart';
                $validatedData['product_id'] = $product_id;
                $validatedData['unique_code'] = $unique_code;
                $validatedData['price_total'] = $price_total;
                Cart::create($validatedData);
                /* hapus data di wishlist */
                Cart::where('product_id', $product_id)
                    ->where('user_id',$user_id)
                    ->where('status','Wishlist')->delete();
                return redirect('/checkout')->with('toast_success', 'Berhasil input keranjang!');
            }

        } else {
            Alert::warning('Out of Stock', 'Maaf jumlah yang diinput melebihi stok yang tersedia');
            return redirect()->back();
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $stock = $request->stock;
        $price = $request->price;
        $qty = $request->quantity;
        $price_total = $price*$qty;

        if ($qty <= $stock) {
            $validatedData = $request->validate([
                'quantity' => 'required|max:255',
            ]);
            $validatedData['price_total'] = $price_total;
            Cart::where('id',$id)->update($validatedData);
            return redirect('/wishlist')->with('toast_success', 'Berhasil update!');
        } else {
            Alert::warning('Out of Stock', 'Maaf jumlah yang diinput melebihi stok yang tersedia, mohon periksa kembali');
            return redirect('/wishlist');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::where('id',$id)->delete();
        return redirect('/wishlist')->with('toast_info', 'Barang telah dihapus dari Wishlist!');
    }

    public function storeWishlist(Request $request)
    {
        // dd($request);
        $id = $request->product_id;
        $cekbrg = Cart::where('user_id',Auth::user()->id)
                ->where('product_id',$id)->where('status','Wishlist')
                ->count();

        /* cek barang serupa */
        if ($cekbrg>0) {
            $wishlist = Cart::where('user_id',Auth::user()->id)
                        ->where('product_id',$id)->where('status','Wishlist')
                        ->first();
            $qty = 1;
            $old_qty = $wishlist->quantity;

            $new_qty = $qty + $old_qty;
            $prc_product = $wishlist->product->get(0)->price;
            $total_harga = $new_qty*$prc_product;

            $validatedData['date'] = Carbon::now();
            $validatedData['quantity'] = $new_qty;
            $validatedData['price_total'] = $total_harga;

            Cart::where('product_id', $id)
                ->where('status','Wishlist')->update($validatedData);
            return redirect('/wishlist')->with('toast_success', 'Berhasil update wishlist!');
        } else {
        /* barang beda */
            $product = Product::where('id',$id)->first();
            $validatedData['product_id'] = $product->id;
            $validatedData['user_id'] = Auth::user()->id;
            $validatedData['quantity'] = '1';
            $validatedData['price_total'] = $product->price;
            $validatedData['date'] = Carbon::now();
            $validatedData['status'] = 'Wishlist';
            Cart::create($validatedData);
            return redirect('/shop')->with('toast_success', 'Berhasil input wishlist!');
        }
    }
}
