<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\User;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $total_harga = Order::where('status','!=','pending')->where('status','!=','failure')->select(DB::raw("CAST(SUM(gross_amount) as int) as gross_amount"))->GroupBy(DB::raw("Month(date)"))->pluck('gross_amount');
        $bulan = Order::where('status','!=','pending')->where('status','!=','failure')->select(DB::raw("MONTHNAME(date) as bulan"))->GroupBy(DB::raw("MONTHNAME(date)"))->pluck('bulan');
        return view('admin.dashboard.index',[
            'active' => 'dashboard',
            'total_harga' => $total_harga,
            'bulan' => $bulan,
            'bank' => Order::whereIn('id',function($query){
                            $query->select('id')->from('payments')->where('payment_type','bank_transfer');
                        })->where('status','!=','failure')->where('status','!=','pending')->sum('gross_amount'),
            'card' => Order::whereIn('id',function($query){
                            $query->select('id')->from('payments')->where('payment_type','credit_card');
                        })->where('status','!=','failure')->where('status','!=','pending')->sum('gross_amount'),
            'wallet'=> Order::whereIn('id',function($query){
                            $query->select('id')->from('payments')->where('payment_type','qris');
                        })->where('status','!=','failure')->where('status','!=','pending')->sum('gross_amount'),
            'store'=>Order::whereIn('id',function($query){
                            $query->select('id')->from('payments')->where('payment_type','cstore');
                        })->where('status','!=','failure')->where('status','!=','pending')->sum('gross_amount'),
            'customers_amount' => User::where('roles','customer')->count(),
            'products_amount' => Product::sum('stock'),
            'sold_quantity' => Cart::whereIn('unique_code',function($query){
                                $query->select('unique_code')->from('orders')->where('status','!=','Pending');
                            })->where('status','!=','Pending')->sum('quantity'),
            'incomes' => Order::where('status','!=','failure')->where('status','!=','pending')->sum('gross_amount'),
            'total_order' => Order::count()
        ]);
    }
    public function print_transactions()
    {
        $start = request('date_start');
        $end = request('date_end');
        $status = request('status');
        if ($status == 'All') {
            $report = Order::whereBetween('date',[$start,$end])->get();
            return view('admin.dashboard.report',[
                'start' => $start,
                'end' => $end,
                'status' => $status,
                'orders' => $report
            ]);
        } else {
            $report = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
            return view('admin.dashboard.report',[
                'start' => $start,
                'end' => $end,
                'status' => $status,
                'orders' => $report
            ]);
        }
    }

    public function export_excel(Request $request)
    {
        $start = $request->date_start;
        $end = $request->date_end;
        $status = $request->status;
        if ($status == 'All') {
            $orders = Order::whereBetween('date',[$start,$end])->get();
        } else {
            $orders = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
        }
        $export = new OrdersExport($orders);
        return Excel::download($export, "order_reports($start _ $end).xlsx");
    }

    public function export_csv(Request $request)
    {
        $start = $request->date_start;
        $end = $request->date_end;
        $status = $request->status;
        if ($status == 'All') {
            $orders = Order::whereBetween('date',[$start,$end])->get();
        } else {
            $orders = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
        }
        $export = new OrdersExport($orders);
        return Excel::download($export, "order_reports($start _ $end).csv");
    }
    public function export_pdf(Request $request)
    {
        $start = $request->date_start;
        $end = $request->date_end;
        $status = $request->status;
        if ($status == 'All') {
            $orders = Order::whereBetween('date',[$start,$end])->get();
        } else {
            $orders = Order::where('status',$status)->whereBetween('date',[$start,$end])->get();
        }
        $export = new OrdersExport($orders);
        return Excel::download($export, "order_reports($start _ $end).pdf");
    }
}
