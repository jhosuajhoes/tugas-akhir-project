<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\VerifyUser;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Validation\Rules\Password;

class RegisterController extends Controller
{
    public function index()
    {
        return view('customer.register');
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4|regex:/^[a-zA-Z]+$/u|max:255',
            'phone_number' => 'required|min:10|max:14|unique:users',
            'email' => 'required|email:dns|unique:users',
            'address' => 'required|min:5|max:255',
            'password' => ['required',Password::min(8)->mixedCase()->letters()->numbers(),],
        ]);

        $validatedData['password'] = bcrypt($validatedData['password']);
        $validatedData['roles'] = 'customer';
        $save = User::create($validatedData);

        $user = User::all()->last();
        $last_id = $user->id;

        $token = $last_id.hash('sha256', Str::random(120));
          $verifyURL = route('verify',['token'=>$token,'service'=>'Email_verification']);

          VerifyUser::create([
              'user_id'=>$last_id,
              'token'=>$token,
          ]);

          $message = 'Dear <b>'.$request->name.'</b>';
          $message.= 'Thanks for signing up, we just need you to verify your email address to complete setting up your account.';

          $mail_data = [
              'recipient'=>$request->email,
              'fromName'=>'Skysea',
              'fromEmail'=>'noreply@gmail.com',
              'subject'=>'Email Verification',
              'body'=>$message,
              'actionLink'=>$verifyURL,
          ];

          Mail::send('customer.email-template', $mail_data, function($message) use ($mail_data){
                     $message->to($mail_data['recipient'])
                             ->from($mail_data['fromEmail'], $mail_data['fromName'])
                             ->subject($mail_data['subject']);
          });

        if ($save) {
            Alert::success('Success', 'You need to verify your account. We have sent you an activation link, please check your email');
            return redirect('/register');
        } else {
            return redirect('/register')->with('fail','Something went wrong, failed to register');
        }

    }
}
