<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Cart;
use App\Models\Size;
use App\Models\Color;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    public function showProduct(Product $product)
    {

        $name = $product->name;
        $category_id = $product->category_id;

        if (Auth::check()) {
            return view('customer.product',[
                'active' => 'shop',
                'wish_count' => Cart::where('user_id',Auth::user()->id)
                ->where('status','=','Wishlist')->count(),
                'wishlist' => Cart::where('user_id',Auth::user()->id)
                ->where('status','=','Wishlist')->get(),
                'count_order' => Order::whereIn('unique_code',function($query){
                    $query->select('unique_code')->from('carts')->where('user_id', Auth::user()->id)->where('status','!=','Cart');
                })->count(),

                'categories' => Category::all(),
                'count' => Cart::where('user_id',Auth::user()->id)
                            ->where('status','=','Cart')->count(),
                'checkouts' => Cart::where('user_id',Auth::user()->id)
                                    ->where('status','=','Cart')->get(),
                'product' => $product,
                'old_code' => Cart::where('user_id',Auth::user()->id)
                                    ->where('status','Cart')->get(),
                'old_code_count' => Cart::where('user_id',Auth::user()->id)
                                    ->where('status','Cart')->count(),
                'qty_old' => Cart::where('user_id',Auth::user()->id)
                                    ->where('product_id',$product->id)
                                    ->where('status','Cart')->get(),
                'qty_old_count' => Cart::where('user_id',Auth::user()->id)
                                    ->where('product_id',$product->id)
                                    ->where('status','Cart')->count(),
                'code' => Str::random(6),
                'products' => Product::where('name',$name)->get(),
                'colors' => Product::where('name',$name)->groupBy('color_id')->get(),
                'sizes' => Product::where('name',$name)->groupBy('size_id')->get()
            ]);
            } else {
                return view('customer.product',[
                    'active' => 'shop',
                    'categories' => Category::all(),
                    'product' => $product,
                    'code' => Str::random(6),
                    'products' => Product::where('name',$name)->get(),
                    'colors' => Product::where('name',$name)->groupBy('color_id')->get(),
                    'sizes' => Product::where('name',$name)->groupBy('size_id')->get()

                ]);
                }
    }

    public function get_stock(Request $request)
    {
        $color = $request->color;
        $size = $request->size;
        $product = Product::where('color_id',$color)->where('size_id',$size)->first();
        if ($request->color && $request->size) {
            echo "$product->stock";
        }
        else {
            echo "";
        }


    }

    public function storeProduct(Request $request)
    {
        // ddd($request);

        $code_id = request('old_code');
        $cek = Cart::where('user_id',Auth::user()->id)
                ->where('status','Cart')->count();
        /* cek stok product */
        $qty = request('quantity');
        $size_id = request('size_id');
        $color_id = request('color_id');
        $stok = request('stok');
        $product_name = request('product_name');
        /* cek produk */
        $cek_produk = Product::where('name',$product_name)
                    ->where('color_id',$color_id)
                    ->where('size_id',$size_id)
                    ->count();
        if ($cek_produk > 0){
            if ($qty <= $stok) {
                /* cek keranjang */
                if ($cek > 0) {
                    $idproduk = Product::where('name',$product_name)
                                        ->where('color_id',$color_id)
                                        ->where('size_id',$size_id)
                                        ->get('id');
                    $id = $idproduk[0]->id;
                    $cekbrg = Cart::where('user_id',Auth::user()->id)
                            ->where('product_id',$id)
                            ->where('unique_code',$code_id)->count();
                    $old_qty = request('qty_old');
                    /* cek barang serupa */
                    if ($cekbrg>0) {
                        $idproduk1 = Product::where('name',$product_name)
                                            ->where('color_id',$color_id)
                                            ->where('size_id',$size_id)
                                            ->get('id');
                        $id1 = $idproduk1[0]->id;
                        $old_qty = Cart::where('user_id',Auth::user()->id)
                                        ->where('product_id',$id1)
                                        ->where('unique_code',$code_id)->get('quantity');
                        $quantity_old = $old_qty[0]->quantity;
                        $new_qty = $qty + $quantity_old;
                        $prc_product = request('price');
                        $total_harga = $new_qty*$prc_product;
                        $validatedData['product_id'] = $id1;
                        $validatedData = $request->validate([
                            'date' => 'required',
                        ]);
                        $validatedData['quantity'] = $new_qty;
                        $validatedData['price_total'] = $total_harga;
                        Cart::where('product_id', $id1)
                            ->where('unique_code',$code_id)->update($validatedData);
                        return redirect('/checkout')->with('toast_success', 'Berhasil update keranjang!');;
                    } else {
                    /* barang beda dengan kode order sama */
                        $code_id = request('old_code');
                        $qty_product = request('quantity');
                        $prc_product = request('price');
                        $total_harga = $qty_product*$prc_product;
                        $idproduk2 = Product::where('name',$product_name)
                                            ->where('color_id',$color_id)
                                            ->where('size_id',$size_id)
                                            ->get('id');
                        $id2 = $idproduk2[0]->id;
                        $validatedData = $request->validate([
                            'quantity' => 'required',
                            'user_id' => 'required',
                            'date' => 'required',
                            'status' => 'required',
                        ]);
                        $validatedData['product_id'] = $id2;
                        $validatedData['unique_code'] = $code_id;
                        $validatedData['price_total'] = $total_harga;
                        Cart::create($validatedData);
                        return redirect('/checkout')->with('toast_success', 'Berhasil input keranjang!');
                    }
                } else {
                    /* belum pernah order */
                    $code = request('code');
                    $qty_product = request('quantity');
                    $prc_product = request('price');
                    $total_harga = $qty_product*$prc_product;
                    $idproduk3 = Product::where('name',$product_name)
                                        ->where('color_id',$color_id)
                                        ->where('size_id',$size_id)
                                        ->get('id');
                    $id3 = $idproduk3[0]->id;
                    $validatedData = $request->validate([
                        'quantity' => 'required',
                        'user_id' => 'required',
                        'date' => 'required',
                        'status' => 'required',
                    ]);
                    $validatedData['product_id'] = $id3;
                    $validatedData['unique_code'] = $code;
                    $validatedData['price_total'] = $total_harga;
                    Cart::create($validatedData);
                    return redirect('/checkout')->with('toast_success', 'Berhasil input keranjang!');
                }
            } else {
                Alert::warning('Out of Stock', 'Maaf jumlah yang diinput melebihi stok yang tersedia');
                return redirect()->back();
            }
        }
        else{
            Alert::warning('Out of Product', 'Maaf produk dengan warna atau ukuran terpilih belum tersedia');
                return redirect()->back();
        }
    }
}
