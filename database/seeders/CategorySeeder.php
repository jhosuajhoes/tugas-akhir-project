<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\City;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();
        $data = [
            'T-Shirt',
            'Pants'
        ];

        foreach ($data as $key => $value) {
            Category::create([
                'name' => $value,
            ]);
        }
    }
}
