<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::truncate();
        $data = [
            'Pink',
            'Tosca',
            'Blue',
            'White',
            'Black',
        ];

        foreach ($data as $key => $value) {
            Color::create([
                'name' => $value,
            ]);
        }
    }
}
