<?php

namespace Database\Seeders;

use App\Models\Size;
use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Size::truncate();
        $data = [
            'S',
            'M',
            'L',
            'XL',
            'XXL'
        ];

        foreach ($data as $key => $value) {
            Size::create([
                'name' => $value,
            ]);
        }
    }
}
