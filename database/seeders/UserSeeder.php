<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::create([
            'name' => 'Jhosua Matindas',
            'phone_number' => '081234567812',
            'email' => 'jhosua@gmail.com',
            'password' => bcrypt('12345'),
            'address' => 'Pogung Lor F9, Sinduadi, Mlati, Sleman YK',
            'roles' => 'admin',
            'email_verified' => 'verified',
        ]);
        User::create([
            'name' => 'Jenny',
            'phone_number' => '08121117812',
            'email' => 'jenn@gmail.com',
            'password' => bcrypt('11111'),
            'address' => 'Jl Letjen MT Haryono Ruko Balikpapan Baru Bl D-1/3,Damai',
            'roles' => 'customer',
            'email_verified' => 'verified',
        ]);
        User::create([
            'name' => 'Rakha',
            'phone_number' => '08121117812',
            'email' => 'rakha@gmail.com',
            'password' => bcrypt('qwerty'),
            'address' => 'Jl Godean Km 5 Yogyakarta',
            'roles' => 'owner',
            'email_verified' => 'verified',
        ]);
    }
}
