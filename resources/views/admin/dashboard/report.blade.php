@extends('admin.admin-layouts.app-reports')
@section('content')
<div class="container">
    <section id="header-kop">
        <div class="container-fluid">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td rowspan="2" width="16%" class="text-center">
                            {{-- <img src="assets/images/fashioninshop.png" alt="fashioninshop.png" width="100px" /> --}}
                        </td>
                        <td class="text-center">
                            <h2>Skysea.co</h2>
                        </td>
                        <td rowspan="3" width="16%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="text-center">Jl. Pandega Marta No.835, Pogung Lor, Sinduadi, Kec.
                            Mlati,
                            Sleman (DIY)</td>
                    </tr>
                </tbody>
            </table>
            <hr>
        </div>
    </section>
    <div class="row">
        <div class="col-12 col-md-6">
            <h5>Transactions Report ({{$status}})</h5>
            <p>From {{$start}} until {{$end}}</p>
        </div>
        <div class="col-12 col-md-6 text-end">
            <div class="btn-group">
                <button class="btn btn-sm btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                    Download
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <form action="/admin/export_excel" method="post">
                        @csrf
                        <input type="hidden" name="date_start" value="{{$start}}">
                        <input type="hidden" name="date_end" value="{{$end}}">
                        <input type="hidden" name="status" value="{{$status}}">
                        <button type="submit" class="dropdown-item">Excel</button>
                    </form>
                    <form action="/admin/export_csv" method="post">
                        @csrf
                        <input type="hidden" name="date_start" value="{{$start}}">
                        <input type="hidden" name="date_end" value="{{$end}}">
                        <input type="hidden" name="status" value="{{$status}}">
                        <button type="submit" class="dropdown-item">CSV</button>
                    </form>
                    <form action="/admin/export_pdf" method="post">
                        @csrf
                        <input type="hidden" name="date_start" value="{{$start}}">
                        <input type="hidden" name="date_end" value="{{$end}}">
                        <input type="hidden" name="status" value="{{$status}}">
                        <button type="submit" class="dropdown-item">PDF</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-sm" id="example">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No</th>
                <th scope="col">Order ID</th>
                {{-- <th scope="col">Transaction ID</th> --}}
                <th scope="col">Customer Name</th>
                <th scope="col">Date</th>
                <th scope="col">Product Name</th>
                <th scope="col">Color</th>
                <th scope="col">Size</th>
                <th scope="col">Qty</th>
                <th scope="col">Total</th>
                <th scope="col">Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td><strong>{{$order->unique_code}}</strong>
                </td>
                {{-- <td>{{$order->transaction_id}}</td> --}}
                <td>{{$order->cart->user->name}}</td>
                <td>{{$order->date}}</td>
                <td>{{$order->cart->product->get(0)->name}}</td>
                <td>{{$order->cart->product->get(0)->color->name}}</td>
                <td>{{$order->cart->product->get(0)->size->name}}</td>
                <td>{{$order->cart->quantity}}</td>
                <td>{{$order->gross_amount}}</td>
                <td>{{$order->status}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="card-body">
    </div>
</div>
@endsection
