@extends('admin.admin-layouts.app')

@section('content')
<div class="content-body">
    <div class="row">
        <div class="col-12">
            <!-- Dashboard Ecommerce Starts -->
            <section id="dashboard-ecommerce">
                <div class="row match-height">
                    <!-- Medal Card -->
                    <div class="col-xl-4 col-md-6 col-12">
                        <div class="card card-congratulation-medal">
                            <div class="card-body">
                                <h5>Welcome's {{auth()->user()->name}}</h5>
                                <p class="card-text font-small-3">Your Total Incomes</p>
                                <h3 class="mb-75 mt-2 pt-50">
                                    <a href="#">IDR{{$incomes}}</a>
                                </h3>
                                <a href="/admin/order" class="btn btn-primary">View Sales</a>
                            </div>
                        </div>
                    </div>
                    <!--/ Medal Card -->

                    <!-- Statistics Card -->
                    <div class="col-xl-8 col-md-6 col-12">
                        <div class="card card-statistics">
                            <div class="card-header">
                                <h4 class="card-title">Statistics</h4>
                                <div class="d-flex align-items-center">
                                    <p class="card-text font-small-2 me-25 mb-0">Updated</p>
                                </div>
                            </div>
                            <div class="card-body statistics-body">
                                <div class="row">
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-success me-2">
                                                <div class="avatar-content">
                                                    <i data-feather='shopping-cart' class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$total_order}}</h4>
                                                <p class="card-text font-small-3 mb-0">Orders</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-info me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="user" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$customers_amount}}</h4>
                                                <p class="card-text font-small-3 mb-0">Customers</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-danger me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="box" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$products_amount}}</h4>
                                                <p class="card-text font-small-3 mb-0">Stocks</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-sm-6 col-12">
                                        <div class="d-flex flex-row">
                                            <div class="avatar bg-light-primary me-2">
                                                <div class="avatar-content">
                                                    <i data-feather="trending-up" class="avatar-icon"></i>
                                                </div>
                                            </div>
                                            <div class="my-auto">
                                                <h4 class="fw-bolder mb-0">{{$sold_quantity}}</h4>
                                                <p class="card-text font-small-3 mb-0">Sold</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Statistics Card -->
                </div>

                <div class="row match-height">
                    <!-- Chart Order Card -->
                    <div class="col-lg-8 col-md-6 col-12">
                        <div class="card card-transaction">
                            <div class="card-header">
                                <h4 class="card-title">Statistics</h4>
                            </div>
                            <div class="card-body">
                                <div id="chart_order"></div>
                            </div>
                        </div>
                    </div>
                    <!--/ Chart Order Card -->

                    <!-- Payment Card -->
                    <div class="col-xl-4 col-md-6 col-12">
                        <div class="card card-transaction">
                            <div class="card-header">
                                <h4 class="card-title">Transactions</h4>
                            </div>
                            <div class="card-body">
                                <div class="transaction-item">
                                    <div class="d-flex">
                                        <div class="avatar bg-light-primary rounded float-start">
                                            <div class="avatar-content">
                                                <i data-feather="pocket" class="avatar-icon font-medium-3"></i>
                                            </div>
                                        </div>
                                        <div class="transaction-percentage">
                                            <h6 class="transaction-title">E-Wallet</h6>
                                            <small>Gopay, Ovo, ShopeePay dll</small>
                                        </div>
                                    </div>
                                    <div class="fw-bolder text-success">+{{$wallet}}</div>
                                </div>
                                <div class="transaction-item">
                                    <div class="d-flex">
                                        <div class="avatar bg-light-success rounded float-start">
                                            <div class="avatar-content">
                                                <i data-feather="dollar-sign" class="avatar-icon font-medium-3"></i>
                                            </div>
                                        </div>
                                        <div class="transaction-percentage">
                                            <h6 class="transaction-title">Bank Transfer</h6>
                                            <small>Bca, Mandiri, Bni, Bri dll</small>
                                        </div>
                                    </div>
                                    <div class="fw-bolder text-success">+{{$bank}}</div>
                                </div>
                                <div class="transaction-item">
                                    <div class="d-flex">
                                        <div class="avatar bg-light-warning rounded float-start">
                                            <div class="avatar-content">
                                                <i data-feather="credit-card" class="avatar-icon font-medium-3"></i>
                                            </div>
                                        </div>
                                        <div class="transaction-percentage">
                                            <h6 class="transaction-title">Credit/Debit Card</h6>
                                            <small>Visa, MasterCard dll</small>
                                        </div>
                                    </div>
                                    <div class="fw-bolder text-success">+{{$card}}</div>
                                </div>
                                <div class="transaction-item">
                                    <div class="d-flex">
                                        <div class="avatar bg-light-info rounded float-start">
                                            <div class="avatar-content">
                                                <i data-feather="shopping-cart" class="avatar-icon font-medium-3"></i>
                                            </div>
                                        </div>
                                        <div class="transaction-percentage">
                                            <h6 class="transaction-title">Convention Store</h6>
                                            <small>Indomart/Alfamart</small>
                                        </div>
                                    </div>
                                    <div class="fw-bolder text-success">+{{$store}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Payment Card -->
                    @can('owner')
                    <!-- Transaction Card -->
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="row match-height">
                            <div class="card card-transaction">
                                <form action="/admin/print-transaction-report" target="_blank" method="GET">
                                    <div class="card-header">
                                        <h4 class="card-title">Report</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12 mb-1">
                                                <label class="form-label" for="fp-default">Start</label>
                                                <input type="date" name="date_start" id="fp-default"
                                                    class="form-control" placeholder="YYYY-MM-DD" required />
                                            </div>
                                            <div class="col-md-12 mb-1">
                                                <label class="form-label" for="fp-default">End</label>
                                                <input type="date" name="date_end" id="fp-default" class="form-control"
                                                    placeholder="YYYY-MM-DD" required />
                                            </div>
                                            <div class="col-md-12 mb-1">
                                                <label class="form-label" for="fp-default">Transaction Status</label>
                                                <select class="form-select" name="status" id="status" required>
                                                    <option value="" holder>- Select Status -</option>
                                                    <option value="All">All</option>
                                                    <option value="delivered">Done (Delivered)</option>
                                                    <option value="settlement">Paid</option>
                                                    <option value="in delivery">In Delivery</option>
                                                    <option value="pending">Pending</option>
                                                    <option value="cancel">Cancel</option>
                                                </select>
                                            </div>

                                        </div>
                                        <button type="submit" class="btn btn-info"><span><i
                                                    data-feather='printer'></i></span> Print</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--/ Transaction Card -->
                    @endcan
                </div>
            </section>
            <!-- Dashboard Ecommerce ends -->
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    var total = {!! json_encode($total_harga) !!};
    var bulan = {!! json_encode($bulan) !!};

    Highcharts.chart('chart_order', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Monthly income'
    },
    xAxis: {
        categories: bulan,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title : {
                text : 'Nominal'
            }
    },
    plotOptions: {
        series: {
                allowPointSelect:true
            }
    },
    series: [{
                name : 'Montly',
                data : total
            }]
});
</script>
@endsection
