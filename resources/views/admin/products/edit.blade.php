@extends('admin.admin-layouts.app')
@section('content')
<div class="content-body">
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Product</h4>
                        <form method="post" action="/admin/product-delete/permanent-delete/{{$product->id}}">
                            @method('delete')
                            @csrf
                            <button class="btn btn-sm btn-danger"
                                onclick="return confirm('Are u sure to delete this product?')">
                                Permanent Delete?
                            </button>
                        </form>
                    </div>
                    <div class="card-body">
                        <form action="/admin/product-update/{{$product->name}}" method="POST" enctype="multipart/form-data"
                            class="form form-horizontal">
                            @method('put')
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="name">Product Name</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" id="name" name="name"
                                                value="{{old('name',$product->name)}}" placeholder="Product name" class="
                                            form-control @error('name') is-invalid @enderror"
                                                data-msg="Please enter Product name"/>
                                            @error('name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="category">Category</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select id="category" name="category_id"
                                                class="select2 form-select @error('category_id') is-invalid @enderror disabled">
                                                @foreach ($categories as $category)
                                                @if (old('category_id',$product->category_id) == $category->id)
                                                <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                                @else
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="price">Price</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="number" id="price" name="price"
                                                class="form-control @error('price') is-invalid @enderror"
                                                value="{{old('price',$product->price)}}" />
                                            @error('price')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="description">Description</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <textarea class="form-control @error('description') is-invalid @enderror"
                                                id="description" name="description" rows="3"
                                                placeholder="Textarea">{{old('description',$product->description)}}</textarea>
                                            @error('description')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 offset-sm-3">
                                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                                    <a href="/admin/product" type="button" class="btn btn-outline-secondary">Back</a>
                                </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
</div>
@endsection
