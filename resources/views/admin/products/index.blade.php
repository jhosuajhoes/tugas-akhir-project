@extends('admin.admin-layouts.app')
@section('content')
<section class="invoice-list-wrapper">
    <div class="card">
        <div class="card-datatable table-responsive">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-center">
                    <h2>Product List</h2>
                    @can('admin')
                    <a href="/admin/product/create" class="btn btn-primary">
                        Input Data
                    </a>
                    @endcan
                </div>
                <hr />
                <div class="data-tables datatable-dark">
                    <table id="orderTable" class="datatables-basic table">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Product</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Description</th>
                                @can('admin')
                                <th>Product Action</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <div class="boxImage">
                                        <a class="linkk" href="/admin/product/{{$product->id}}/image-change">
                                            <img class="img-fluid" width="80px" height="80px"
                                                src="{{ asset('storage/' . $product->image)}}"><br>
                                            <i class="iconsw overlayy" data-feather='edit-2'></i>
                                        </a>
                                    </div>
                                </td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->category->name}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->where('name',$product->name)->sum('stock')}}</td>
                                <td>{{Str::substr($product->description,0,35)}}...</td>
                                @can('admin')
                                <td>
                                    <form method="post" action="/admin/product-delete/{{$product->id}}">
                                        <a href="/admin/product-show/{{$product->name}}" class="badge bg-info"><span
                                                data-feather="eye"></span></a>
                                        <a href="/admin/product/{{$product->name}}/edit-all" class="badge bg-warning"><span
                                                data-feather="edit"></span></a>
                                        @method('delete')
                                        @csrf
                                        <button class="badge bg-danger border-0"
                                            onclick="return confirm('Are u sure to delete this product?')">
                                            <span data-feather="x-circle"></span>
                                        </button>
                                    </form>
                                </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Product</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Description</th>
                                @can('admin')
                                <th>Product Action</th>
                                @endcan
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


@endsection
