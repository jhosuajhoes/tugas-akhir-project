@extends('admin.admin-layouts.app')
@section('content')
<div class="content-body">
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Insert Product</h4>
                    </div>
                    <div class="card-body">
                        <form action="/admin/product" method="POST" enctype="multipart/form-data"
                            class="form form-horizontal">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="name">Product Name</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" id="name" name="name" value="{{old('name')}}"
                                                placeholder="Product name" class="
                                            form-control @error('name') is-invalid @enderror"
                                                data-msg="Please enter Product name" />
                                            @error('name')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="category">Category</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select id="category" name="category_id"
                                                class="select2 form-select @error('category_id') is-invalid @enderror"
                                                value="{{old('category_id')}}">
                                                @foreach ($categories as $category)
                                                @if (old('category_id') == $category->id)
                                                <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                                @else
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="color">Colors</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select id="color" name="color_id"
                                                class="select2 form-select @error('color_id') is-invalid @enderror"
                                                value="{{old('color_id')}}">
                                                @foreach ($colors as $color)
                                                @if (old('color_id') == $color->id)
                                                <option value="{{$color->id}}" selected>{{$color->name}}</option>
                                                @else
                                                <option value="{{$color->id}}">{{$color->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('color_id')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="size">Sizes</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select id="size" name="size_id"
                                                class="select2 form-select @error('size_id') is-invalid @enderror"
                                                value="{{old('size_id')}}">
                                                @foreach ($sizes as $size)
                                                @if (old('size_id') == $size->id)
                                                <option value="{{$size->id}}" selected>{{$size->name}}</option>
                                                @else
                                                <option value="{{$size->id}}">{{$size->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('size_id')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="price">Price</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="number" id="price" name="price"
                                                class="form-control @error('price') is-invalid @enderror"
                                                value="{{old('price')}}" />
                                            @error('price')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="stock">Stock</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="number" id="stock" name="stock"
                                                class="form-control @error('stock') is-invalid @enderror"
                                                value="{{old('stock')}}" />
                                            @error('stock')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="description">Description</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <textarea class="form-control @error('description') is-invalid @enderror"
                                                id="description" name="description" rows="3"
                                                placeholder="Textarea">{{old('description')}}</textarea>
                                            @error('description')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="image">Image</label>
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="file" id="image" name="image"
                                                class="form-control @error('image') is-invalid @enderror" />
                                            @error('image')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 offset-sm-3">
                                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                                    <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
</div>
@endsection
