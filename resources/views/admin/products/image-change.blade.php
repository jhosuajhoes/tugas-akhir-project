@extends('admin.admin-layouts.app')
@section('content')
<div class="content-body">
    <section id="description-list-alignment">
        <div class="row match-height">
            <div class="col-md-5 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Product Image <small class="text-muted">change</small></h4>
                    </div>
                    <div class="card-body">
                        <div class="row p-1">
                            <div class="col-sm-12 col-12">
                                <form action="/admin/product/{{$product->id}}/image-change" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <img class="img-fluid" id="imgInput" src="{{ asset('storage/' . $product->image)}}"
                                        alt="Preview Image" />
                                    <hr>
                                    <input type="file" id="image" name="image"
                                        class="form-control @error('image') is-invalid @enderror"
                                        onchange="loadFile(event)" />

                                    @error('image')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                    <hr>
                                    <button type="submit" class="btn btn-primary me-1">Change</button>
                                    <a href="/admin/product" class="btn btn-outline-secondary">Back</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
</div>
@endsection
