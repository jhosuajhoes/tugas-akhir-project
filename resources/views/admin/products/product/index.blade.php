@extends('admin.admin-layouts.app')
@section('content')
<section class="invoice-list-wrapper">
    <div class="row match-height">
        <div class="col-lg-8 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <div class="card-body">
                        <div class="d-sm-flex justify-content-between align-items-center">
                            <h4 class="card-title">Product : {{$product->name}}</h4>
                        </div>
                        <div class="data-tables datatable-dark">
                            <table id="orderTable" class="datatables-basic table" style="width:100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$product->color->name}}</td>
                                        <td>{{$product->size->name}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>{{$product->stock}}</td>
                                        <td>
                                            <form method="post" action="/admin/product/{{$product->id}}">
                                                <a href="/admin/product/{{$product->id}}" class="badge bg-info"><span
                                                        data-feather="eye"></span></a>
                                                <a href="/admin/product/{{$product->id}}/edit"
                                                    class="badge bg-warning"><span data-feather="edit"></span></a>
                                                @method('delete')
                                                @csrf
                                                <input type="hidden" name="name" value="{{$product->name}}">
                                                <button class="badge bg-danger border-0"
                                                    onclick="return confirm('Delete data?')">
                                                    <span data-feather="x-circle"></span>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="card card-developer-meetup">
                <div class="meetup-img-wrapper rounded-top text-center">
                    <img src="{{ asset('storage/' . $product->image)}}" alt="Meeting Pic" height="150" />
                </div>
                <div class="card-body">
                    <h4 class="card-title">Insert new variant</h4>
                    <form action="/admin/product/create-variant" method="POST" enctype="multipart/form-data"
                        class="form form-horizontal">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="form-label" for="color">Colors</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="color" name="color_id"
                                            class="select2 form-select @error('color_id') is-invalid @enderror"
                                            value="{{old('color_id')}}">
                                            @foreach ($colors as $color)
                                            @if (old('color_id') == $color->id)
                                            <option value="{{$color->id}}" selected>{{$color->name}}</option>
                                            @else
                                            <option value="{{$color->id}}">{{$color->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @error('color_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="form-label" for="size">Sizes</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select id="size" name="size_id"
                                            class="select2 form-select @error('size_id') is-invalid @enderror"
                                            value="{{old('size_id')}}">
                                            @foreach ($sizes as $size)
                                            @if (old('size_id') == $size->id)
                                            <option value="{{$size->id}}" selected>{{$size->name}}</option>
                                            @else
                                            <option value="{{$size->id}}">{{$size->name}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        @error('size_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="form-label" for="stock">Stock</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="number" id="stock" name="stock"
                                            class="form-control @error('stock') is-invalid @enderror" required
                                            value="{{old('stock')}}" />
                                        @error('stock')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <input type="hidden" name="name" value="{{$product->name}}">
                                <input type="hidden" name="category_id" value="{{$product->category_id}}">
                                <input type="hidden" name="price" value="{{$product->price}}">
                                <input type="hidden" name="description" value="{{$product->description}}">
                                <input type="hidden" name="image" value="{{$product->image}}">

                                <div class="col-sm-12 text-end">
                                    <button type="submit" class="btn btn-sm btn-primary me-1">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


@endsection
