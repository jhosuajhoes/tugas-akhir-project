@extends('admin.admin-layouts.app')
@section('content')
<div class="content-body">
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-8 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Product</h4>
                        <form method="post" action="/admin/product/permanent-delete/{{$product->id}}">
                            @method('delete')
                            @csrf
                            <button class="badge bg-danger border-0"
                                onclick="return confirm('Delete data permanent?')">
                                Permanent delete?
                            </button>
                        </form>
                    </div>
                    <div class="card-body">
                        <form action="/admin/product/{{$product->id}}" method="POST" enctype="multipart/form-data"
                            class="form form-horizontal">
                            @method('put')
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="name">Product Name</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <h4>{{$product->name}}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="color">Colors</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select id="color" name="color_id"
                                                class="select2 form-select @error('color_id') is-invalid @enderror"
                                                value="{{old('color_id',$product->color->name)}}">
                                                @foreach ($colors as $color)
                                                @if (old('color_id', $product->color_id) == $color->id)
                                                <option value="{{$color->id}}" selected>{{$color->name}}</option>
                                                @else
                                                <option value="{{$color->id}}">{{$color->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('color_id')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="size">Sizes</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select id="size" name="size_id"
                                                class="select2 form-select @error('size_id') is-invalid @enderror"
                                                value="{{old('size_id',$product->size->name)}}">
                                                @foreach ($sizes as $size)
                                                @if (old('size_id',$product->size_id) == $size->id)
                                                <option value="{{$size->id}}" selected>{{$size->name}}</option>
                                                @else
                                                <option value="{{$size->id}}">{{$size->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('size_id')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="form-label" for="stock">Stock</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="number" id="stock" name="stock"
                                                class="form-control @error('stock') is-invalid @enderror"
                                                value="{{old('stock',$product->stock)}}" />
                                            @error('stock')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 offset-sm-3">
                                    <input type="hidden" name="name" value="{{$product->name}}">
                                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
</div>
@endsection
