@extends('admin.admin-layouts.app')
@section('content')
<section class="invoice-list-wrapper">
    <div class="card">
        <div class="card-datatable table-responsive">
            <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-center">
                    <h2>Product Size</h2>
                    @can('admin')
                    <a class="btn btn-primary" href="/admin/size/create" role="button">Input Data</a>
                    @endcan
                </div>
                <hr />
                <div class="data-tables datatable-dark">
                    <table id="orderTable" class="datatables-basic table" style="width:100%">
                        <thead class="thead-dark">
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Product in size</th>
                                @can('admin')
                                <th>Action</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sizes as $size)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$size->name}}</td>
                                <td>{{$product->where('size_id',$size->id)->count()}}</td>
                                @can('admin')
                                <td>
                                    <form method="post" action="/admin/size/{{$size->id}}">
                                    <a href="/admin/size/{{$size->id}}/edit" class="badge bg-warning"><span
                                            data-feather="edit"></span></a>
                                        @method('delete')
                                        @csrf
                                        <button class="badge bg-danger border-0"
                                            onclick="return confirm('Delete data?')">
                                            <span data-feather="x-circle"></span>
                                        </button>
                                    </form>
                                </td>
                                @endcan
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection
