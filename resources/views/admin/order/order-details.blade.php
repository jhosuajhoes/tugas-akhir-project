@extends('admin.admin-layouts.app')
@section('content')
<!-- BEGIN: Content-->
<section class="invoice-preview-wrapper">
    <div class="row invoice-preview">
        <!-- Invoice -->
        <div class="col-xl-9 col-md-8 col-12">
            <div class="card invoice-preview-card">
                <div class="card-body invoice-padding pb-0">
                    <!-- Header starts -->
                    <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                        <div>
                            <div class="logo-wrapper">
                                <h3 class="text-primary invoice-logo">Skysea.co</h3>
                            </div>
                            <p class="card-text mb-25">Pogung Lor Office, Sinduadi, Sleman</p>
                            <p class="card-text mb-25">Yogyakarta, 55284, Indonesia</p>
                            <p class="card-text mb-0">+62 822 541 682 12</p>
                        </div>
                        <div class="mt-md-0 mt-2">
                            <h4 class="invoice-title">
                                Invoice
                                <span class="invoice-number">#{{$order->unique_code}}</span>
                            </h4>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Date Order:</p>
                                <p class="invoice-date">{{$order->date}}</p>
                            </div>
                            <div class="invoice-date-wrapper">
                                <p class="invoice-date-title">Status:</p>
                                <p class="invoice-date">{{$order->status}}</p>
                            </div>
                        </div>
                    </div>
                    <!-- Header ends -->
                </div>

                <hr class="invoice-spacing" />

                <!-- Address and Contact starts -->
                <div class="card-body invoice-padding pt-0">
                    <div class="row invoice-spacing">
                        <div class="col-xl-8 p-0">
                            <h6 class="mb-2">Invoice To:</h6>
                            <h6 class="mb-25">{{$order->cart->user->name}}</h6>
                            <p class="card-text mb-25">Customer</p>
                            <p class="card-text mb-25">{{$order->cart->user->address}}</p>
                            <p class="card-text mb-25">{{$order->cart->user->phone_number}}</p>
                            <p class="card-text mb-0">{{$order->cart->user->email}}</p>
                        </div>
                        <div class="col-xl-4 p-0 mt-xl-0 mt-2">
                            <h6 class="mb-2">Payment Details:</h6>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="pe-1">Total Due:</td>
                                        <td><span class="fw-bold">{{$order->payment->gross_amount}}</span></td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">Payment:</td>
                                        <td>{{$order->payment->payment_type}}</td>
                                    </tr>
                                    <tr>
                                        <td class="pe-1">Country:</td>
                                        <td>Indonesia</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Address and Contact ends -->

                <!-- Invoice Description starts -->
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="py-1">Product description</th>
                                <th class="py-1">Color</th>
                                <th class="py-1">Size</th>
                                <th class="py-1">Qty</th>
                                <th class="py-1">Price</th>
                                <th class="py-1">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($order_cart as $result)
                            <tr class="border-bottom">
                                <td class="py-1">
                                    <p class="card-text fw-bold mb-25">{{$result->product->get(0)->name}}</p>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->product->get(0)->color->name}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->product->get(0)->size->name}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->quantity}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->product->get(0)->price}}</span>
                                </td>
                                <td class="py-1">
                                    <span class="fw-bold">{{$result->quantity * $result->product->get(0)->price}}</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-body invoice-padding pb-0">
                    <div class="row invoice-sales-total-wrapper">
                        <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
                            <p class="card-text mb-0">
                                <span class="fw-bold">Admin:</span> <span class="ms-75">{{auth()->user()->name}}</span>
                            </p>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
                            <div class="invoice-total-wrapper">
                                <div class="invoice-total-item">
                                    <p class="invoice-total-title">Courier:</p>
                                    <p class="invoice-total-amount">{{$order->shipment->courier}}</p>
                                </div>
                                <div class="invoice-total-item">
                                    <p class="invoice-total-title">Shipping cost:</p>
                                    <p class="invoice-total-title text-warning">{{$order->shipment->shipping_cost}}</p>
                                </div>
                                <hr class="my-50" />
                                <div class="invoice-total-item">
                                    <p class="invoice-total-title">Total:</p>
                                    <p class="invoice-total-amount">{{$order->payment->gross_amount}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Invoice Description ends -->

                <hr class="invoice-spacing" />

                <!-- Invoice Note starts -->
                <div class="card-body invoice-padding pt-0">
                    <div class="row">
                        <div class="col-12">
                            <span class="fw-bold">Note:</span>
                            <span>Thank you for purchasing our products, we hope that the products we provide can provide comfort to our customers.
                                <br>
                                *Please keep this invoice properly.</span>
                        </div>
                    </div>
                </div>
                <!-- Invoice Note ends -->
            </div>
        </div>
        <!-- /Invoice -->
        @can('admin')
        <!-- Invoice Actions -->
        <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
            <div class="card">
                <div class="card-body">
                    @if ($order->status == 'settlement')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        <input type="hidden" name="status" value="in delivery">
                        <button type="submit" class="btn btn-primary w-100 mb-75">
                            Set to Delivery
                        </button>
                    </form>
                    @elseif ($order->status == 'in delivery')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        <input type="hidden" name="status" value="delivered">
                        <button type="submit" class="btn btn-secondary w-100 mb-75">
                            Set to Done
                        </button>
                    </form>
                    @elseif ($order->status == 'pending')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        @foreach ($order_cart as $value)
                        <input type="hidden" name="qty[]" value="{{$value->quantity}}">
                        <input type="hidden" name="stock[]" value="{{$value->product->get(0)->stock}}">
                        <input type="hidden" name="product_id[]" value="{{$value->product->get(0)->id}}">
                        @endforeach
                        <input type="hidden" name="status" value="failure">
                        <button type="submit" class="btn btn-danger w-100 mb-75">
                           Cancel order
                        </button>
                    </form>
                    @elseif ($order->status == 'delivered')
                    <span class="badge rounded-pill badge-light-success me-1 mt-1 mb-2">Order has been completed</span>
                    @else
                    <a href="#" class="btn btn-warning w-100 btn-download-invoice mb-75">Order canceled!</a>
                    @endif
                    <a class="btn btn-outline-secondary w-100 mb-75" href="/admin/order-invoice/{{$order->unique_code}}"
                        target="_blank">
                        Print </a>
                </div>
            </div>
        </div>
        <!-- /Invoice Actions -->
        @endcan
    </div>
</section>
@endsection
