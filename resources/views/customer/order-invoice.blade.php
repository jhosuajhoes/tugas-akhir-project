@extends('admin.admin-layouts.app-invoice')
@section('content')
<div class="invoice-print p-3">
    <div class="invoice-header d-flex justify-content-between flex-md-row flex-column pb-2">
        <div>
            <div class="logo-wrapper">
                <h3 class="text-primary invoice-logo">Skysea.co</h3>
            </div>
            <p class="card-text mb-25">Pogung Lor Office, Sinduadi, Sleman</p>
            <p class="card-text mb-25">Yogyakarta, 55284, Indonesia</p>
            <p class="card-text mb-0">+62 822 541 682 12</p>
        </div>
        <div class="mt-md-0 mt-2">
            <h4 class="invoice-title">
                Invoice
                <span class="invoice-number">#{{$order->unique_code}}</span>
            </h4>
            <div class="invoice-date-wrapper">
                <p class="invoice-date-title">Date Order</p>
                <p class="invoice-date">: {{$order->date}}</p>
            </div>
            @if ($order->status == 'settlement')
            <div class="invoice-date-wrapper">
                <p class="invoice-date-title">Status</p>
                <p class="invoice-date">:  <span class="badge badge-light-success">On Process</span></p>
            </div>
            @elseif ($order->status == 'pending')
            <div class="invoice-date-wrapper">
                <p class="invoice-date-title">Status</p>
                <p class="invoice-date">:  <span class="badge badge-light-warning">Waiting payment</span></p>
            </div>
            @elseif ($order->status == 'delivered')
            <div class="invoice-date-wrapper">
                <p class="invoice-date-title">Status</p>
                <p class="invoice-date">:  <span class="badge badge-light-success">Order Complete</span></p>
            </div>
            @else
            <div class="invoice-date-wrapper">
                <p class="invoice-date-title">Status</p>
                <p class="invoice-date">:  <span class="badge badge-light-danger">Cancel</span></p>
            </div>
            @endif
            <div class="invoice-date-wrapper">
                <p class="invoice-date-title">Customer</p>
                <p class="invoice-date fw-bolder">: {{$order->cart->user->name}}</p>
            </div>
        </div>
    </div>

    <hr class="my-2" />

    <div class="row pb-2">
        <div class="col-sm-6">
            <h6 class="mb-1">Shipping Details:</h6>
            <table>
                <tbody>
                    <tr>
                        <td class="pe-1">Courier</td>
                        <td><span class="text-uppercase">: {{$order->shipment->courier}}</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="pe-1"> Shipping Address</td>
                        <td>: {{$order->shipment->address}}</td>
                    </tr>
                    <tr>
                        <td class="pe-1"></td>
                        <td>{{$order->shipment->city->city_name}}, {{$order->shipment->city->province->province}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-6 mt-sm-0 mt-2">
            <h6 class="mb-1">Payment Details:</h6>
            <table class="fs-14">
                <tbody>
                    <tr>
                        <td class="pe-1">Total Due</td>
                        <td><span class="fw-bold">: {{$order->payment->gross_amount}}</span>
                        </td>
                    </tr>
                    @if ($order->payment->payment_type == 'cstore')
                        <tr>
                            <td class="pe-1">Payment Code</td>
                            <td class="text-uppercase">: {{$order->payment->payment_code}}</td>
                        </tr>
                        <tr>
                            <td class="pe-1">Payment type</td>
                            <td class="text-uppercase">: {{$order->payment->payment_type}}</td>
                        </tr>
                    @elseif ($order->payment->payment_type == 'bank_transfer')
                        @if ($order->status == 'pending')
                        <tr>
                            <td class="pe-1"><span class="text-warning"><strong>Please Transfer to</strong></span></td>
                            <td class="text-uppercase"></td>
                        </tr>
                        <tr>
                            <td class="pe-1"><span>Virtual Account No. </span></td>
                            <td class="text-uppercase fw-bolder">: {{$order->payment->va_number}} ({{$order->payment->bank}})</td>
                        </tr>
                        @else
                        <tr>
                            <td class="pe-1">Payment type</td>
                            <td class="text-uppercase">: {{$order->payment->payment_type}}
                                ({{$order->payment->bank}})</td>
                        </tr>
                        <tr>
                            <td class="pe-1">No.Virtual Account</td>
                            <td class="text-uppercase">: {{$order->payment->va_number}}</td>
                        </tr>
                        @endif
                    @else
                    <tr>
                        <td class="pe-1">Payment type</td>
                        <td class="text-uppercase">: {{$order->payment->payment_type}}</td>
                    </tr> @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-sm" style="font-size: 13px">
            <thead>
                <tr>
                    <th class="py-1">Product description</th>
                    <th class="py-1">Color</th>
                    <th class="py-1">Size</th>
                    <th class="py-1">Qty</th>
                    <th class="py-1">Price</th>
                    <th class="py-1">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order_cart as $result)
                <tr class="border-bottom">
                    <td class="py-1">
                        <p class="card-text fw-bold mb-25">{{$result->product->get(0)->name}}</p>
                    </td>
                    <td class="py-1">
                        <span class="fw-bold">{{$result->product->get(0)->color->name}}</span>
                    </td>
                    <td class="py-1">
                        <span class="fw-bold">{{$result->product->get(0)->size->name}}</span>
                    </td>
                    <td class="py-1">
                        <span class="fw-bold">{{$result->quantity}}</span>
                    </td>
                    <td class="py-1">
                        <span class="fw-bold">{{$result->product->get(0)->price}}</span>
                    </td>
                    <td class="py-1">
                        <span class="fw-bold">{{$result->quantity * $result->product->get(0)->price}}</span>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="row col-12 invoice-sales-total-wrapper mt-1 mb-1">
        <div class="col-md-12 d-flex justify-content-end order-md-2 order-1">
            <div class="invoice-total-wrapper">
                <div class="invoice-total-item">
                    <p class="invoice-total-title">Courier:</p>
                    <p class="invoice-total-amount text-uppercase">{{$order->shipment->courier}}</p>
                </div>
                <div class="invoice-total-item">
                    <p class="invoice-total-title">Shipping cost:</p>
                    <p class="invoice-total-amount">{{$order->shipment->shipping_cost}}</p>
                </div>
                <hr class="my-50" />
                <div class="invoice-total-item">
                    <p class="invoice-total-title">Total:</p>
                    <p class="invoice-total-amount">{{$order->payment->gross_amount}}</p>
                </div>
            </div>
        </div>
    </div>

    <hr class="my-2" />

    <div class="row">
        <div class="col-12">
            <span class="fw-bold">Note:</span>
            <span>Thank you for purchasing our products, we hope that the products we provide can provide comfort to our
                customers.
                <br>
                *Please keep this invoice properly.</span>
        </div>
    </div>
</div>

@endsection
