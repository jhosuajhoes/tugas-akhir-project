@extends('partial.loginApp')

@section('content')

<!-- BEGIN: Content-->
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="auth-wrapper auth-basic px-2">
                <div class="auth-inner my-2">
                    <!-- Login basic -->
                    <div class="card mb-0">
                        <div class="card-header">
                            {{-- @if(session()->has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('success') }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            @if(session()->has('info'))
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    {{ session('info') }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            @if(session()->has('loginError'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{ session('loginError') }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif --}}
                        </div>
                        <div class="card-body">
                            <a href="/" class="brand-logo">
                                <h2 class="brand-text text-primary ms-1">Skysea.co</h2>
                            </a>
                            <h4 class="card-title mb-1">Welcome in Skysea.co 👋</h4>
                            <p class="card-text mb-2">Please login or create an account to start shopping</p>

                            <form class="auth-login-form mt-2" action="/login" method="POST">
                                @csrf
                                <div class="mb-1">
                                    <label for="login-email" class="form-label">Email</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="login-email" name="email" placeholder="john@example.com" aria-describedby="login-email" value="{{Session::get('verifiedEmail') ? Session::get('verifiedEmail') : old('email')}}" required autofocus />
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="mb-1">
                                    <div class="d-flex justify-content-between">
                                        <label class="form-label" for="login-password">Password</label>
                                        <a href="{{route('forgot.password.form')}}">
                                            <small>Forgot Password?</small>
                                        </a>
                                    </div>
                                    <div class="input-group input-group-merge form-password-toggle">
                                        <input type="password" class="form-control form-control-merge" id="login-password" name="password" tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="login-password" required />
                                        <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary w-100 mt-2" tabindex="4">Sign in</button>
                            </form>
                            <a href="{{route('google.login')}}" class="btn btn-md btn-outline-secondary w-100">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-google" viewBox="0 0 16 16">
                                    <path
                                        d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301 1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z" />
                                </svg>
                                <span class="m-l-5">Sign in with Google</span>
                            </a>
                            <p class="text-center mt-2">
                                <span>Don't have an account?</span>
                                <a href="/register">
                                    <span>Sign up</span>
                                </a>
                            </p>
                            <div class="divider my-2">
                                <div class="divider-text">Our Contacts</div>
                            </div>

                            <div class="auth-footer-btn d-flex justify-content-center">
                                <a href="https://wa.me/6282254168212" target="_blank" class="btn btn-success">
                                    <i data-feather="phone-call"></i>
                                </a>
                                <a href="https://www.instagram.com/skysea.co/" target="_blank" class="btn btn-twitter white">
                                    <i data-feather="instagram"></i>
                                </a>
                                <a href="mailto:https://shutx2306@gmail.com" target="_blank"class="btn btn-google">
                                    <i data-feather="mail"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- /Login basic -->
                </div>
            </div>

        </div>
    </div>

@endsection
<!-- END: Content-->
