@extends('partial.app')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12 d-flex">
                                <h2 class="content-header-title float-start mb-0">Product Details</h2>
                                <div class="breadcrumb-wrapper">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#">eCommerce</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="app-ecommerce-shop.html">Shop</a>
                                        </li>
                                        <li class="breadcrumb-item active">Details
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    <!-- app e-commerce details start -->
                    <section class="app-ecommerce-details">
                        <div class="card">
                            <!-- Product Details starts -->
                            <form method="POST" action="/product">
                                @csrf
                                <div class="card-body">
                                    <div class="row my-2">
                                        <div
                                            class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                            <div class="d-flex align-items-center justify-content-center">
                                                <img src="{{ asset('storage/' . $product->image)}}"
                                                    class="img-fluid product-img" alt="product image" />
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-7">
                                            <h4>{{$product->name}}</h4>
                                            <span class="card-text item-company">By <a href="#"
                                                    class="company-name">Skysea.co</a></span>
                                            <div class="ecommerce-details-price d-flex flex-wrap mt-1">
                                                <h4 class="item-price me-1">Rp{{$product->price}}</h4>
                                                @if ($product->rate === '1')
                                                <ul class="unstyled-list list-inline">
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                </ul>
                                                @elseif ($product->rate === '2')
                                                <ul class="unstyled-list list-inline">
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                </ul>
                                                @elseif ($product->rate === '3')
                                                <ul class="unstyled-list list-inline">
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                </ul>
                                                @elseif ($product->rate === '4')
                                                <ul class="unstyled-list list-inline">
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="unfilled-star"></i></li>
                                                </ul>
                                                @elseif ($product->rate === '5')
                                                <ul class="unstyled-list list-inline">
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star"
                                                            class="filled-star"></i></li>
                                                </ul>
                                                @endif
                                            </div>
                                            <p class="card-text">Available - <span class="text-success"><strong
                                                        id="stock"></strong> In
                                                    stock</span></p>
                                            <p class="card-text">
                                                {{$product->description}}
                                            </p>
                                            <hr />
                                            <div class="row">
                                                <div class="mb-1 col-3">
                                                    <h6>Colors</h6>
                                                    <select name="color_id" class="form-select" id="color" required>
                                                        <option value="" holder>Pilih Warna</option>
                                                        @foreach ($colors as $color)
                                                        <option value="{{$color->color->id}}">{{$color->color->name}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="mb-1 col-3">
                                                    <h6>Size</h6>
                                                    <select name="size_id" class="form-select" id="size" required>
                                                        <option value="" holder>Pilih Ukuran</option>
                                                        @foreach ($sizes as $size)
                                                        <option value="{{$size->size->id}}">{{$size->size->name}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <ul class="product-features list-unstyled">
                                                <li><i data-feather="truck"></i> <span>Fast delivery</span></li>
                                                <li>
                                                    <i data-feather="dollar-sign"></i>
                                                    <span>Low Price with High Quality</span>
                                                </li>
                                            </ul>
                                            <hr />
                                            <div class="item-quantity d-flex">
                                                <span class="quantity-title">Qty</span>
                                                <div class="quantity-counter-wrapper">
                                                    <div class="input-group">
                                                        <input type="number" min="1" name="quantity" onkeypress="return hanyaAngka(event)"
                                                            class="quantity-counter" required />
                                                    </div>
                                                </div>
                                            </div>

                                            <hr />

                                            <input type="hidden" name="stok" value="{{$product->stock}}">

                                            <input type="hidden" name="product_name" value="{{$product->name}}">
                                            @auth
                                            @if ($old_code_count == 0)
                                            <input type="hidden" name="code" id="" value="{{$code}}">
                                            @else
                                            <input type="hidden" name="old_code" id=""
                                                value="{{$old_code->get(0)->unique_code}}">
                                            @endif
                                            <input type="hidden" name="user_id" id="" value="{{auth()->user()->id}}">
                                            @if ($qty_old_count == 0)
                                            @else
                                            <input type="hidden" name="qty_old" id=""
                                                value="{{$qty_old->get(0)->quantity}}">
                                            @endif
                                            @else
                                            @endauth

                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <input type="hidden" name="price" value="{{$product->price}}">
                                            <input type="hidden" name="status" value="Cart">
                                            <input type="hidden" name="date" value="<?php echo date("Y-m-d"); ?>">
                                            <div class="d-flex flex-column flex-sm-row pt-1">
                                                @auth
                                                <button type="submit" class="btn btn-primary me-0 me-sm-1 mb-1 mb-sm-0">
                                                    <i data-feather="shopping-cart" class="me-50"></i>
                                                    <span>Add to cart</span>
                                                </button>
                                                @else
                                                <button type="submit" class="btn btn-primary me-0 me-sm-1 mb-1 mb-sm-0">
                                                    <i data-feather="shopping-cart" class="me-50"></i>
                                                    <span>Add to cart</span>
                                                </button>
                                                @endauth
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <!-- Product Details ends -->

                            <!-- Related Products starts -->
                            {{-- <div class="card-body border-top">
                                <div class="mt-4 mb-2 text-center">
                                    <h4>Related Products</h4>
                                    <p class="card-text">People also search for this items</p>
                                </div>
                                <div class="swiper-responsive-breakpoints swiper-container px-4 py-2">
                                    <div class="swiper-wrapper">
                                        @foreach ($relates as $val)
                                        <div class="swiper-slide">
                                            <a href="#">
                                                <div class="item-heading">
                                                    <h5 class="text-truncate mb-0">{{$relates->get(0)->name}}</h5>
                        </div>
                        <div class="img-container w-50 mx-auto py-75">
                            <img src="{{ asset('storage/' . $relates->get(0)->image)}}" class="img-fluid product-img"
                                alt="product image" />
                        </div>
                        <div class="item-meta">
                            <ul class="unstyled-list list-inline mb-25">
                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                            </ul>
                            <p class="card-text text-primary mb-0">$399.98</p>
                        </div>
                        </a>
                </div>
                @endforeach
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div> --}}
    <!-- Related Products ends -->
</div>
</section>
<!-- app e-commerce details end -->

</div>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" language="JavaScript">
    $(function () {
        $('#size').on('change', function () {
            let color = $('#color').val();
            let size = $('#size').val();
            $.ajax({
                url: "{{route('getStock')}}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    color: color,
                    size: size
                },
                cache: false,
                success: function (msg) {
                    $('#stock').html(msg);
                },
                error: function (data) {
                    console.log('error:', data)
                },
            });
        });
    });

</script>

@endsection
