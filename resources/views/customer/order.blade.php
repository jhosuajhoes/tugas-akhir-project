@extends('partial.app')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Daftar Order</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Order
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row p-2">
                @if ($count_order >= 1)
                <div class="col-12">
                    @foreach ($carts as $cart)
                    <div class="card p-1">
                        <div class="row col-12 d-flex mb-1">
                            <div class="author-info">
                                <small class="text-muted me-25">{{$cart->order->get(0)->date}}</small>
                                @if ($cart->order->get(0)->status == 'settlement')
                                <small class="badge badge-light-warning">On Process</small>
                                @elseif ($cart->order->get(0)->status == 'capture')
                                <small class="badge badge-light-warning">On Process</small>
                                @elseif ($cart->order->get(0)->status == 'pending')
                                    @if ($cart->order->get(0)->payment->payment_type == 'cstore')
                                    <a target="_blank" href="{{$cart->order->get(0)->payment->invoice}}">
                                        <small class="badge badge-light-info">Please Complete Payment</small>
                                    </a>
                                    @else
                                    <a href="/order-details/{{$cart->unique_code}}">
                                        <small class="badge badge-light-info">Please Complete Payment</small>
                                    </a>
                                    @endif
                                @elseif ($cart->order->get(0)->status == 'in delivery')
                                <small class="badge badge-light-warning">on delivery</small>
                                @elseif ($cart->order->get(0)->status == 'delivered')
                                <small class="badge badge-light-success">Order Complete</small>
                                @else
                                <small class="badge badge-light-danger">Canceled</small>
                                @endif
                                <span class="text-muted ms-50 me-25">|</span>
                                <small class="text-muted">#{{$cart->order->get(0)->unique_code}}</small>
                                <div class="text-end">
                                    @if ($cart->order->get(0)->status == 'in delivery')
                                    <small class="text-muted">Estimated Arrival {{$cart->order->get(0)->shipment->arrival_date}}</small>
                                    <br>
                                    <small class="text-warning">Please confirm if the product has arrived</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row col-12">
                            <div class="col-md-10 border-end">
                                <div class="row col-12">
                                    @foreach ($products as $val)
                                    @if ($val->unique_code == $cart->unique_code)
                                    <div class="col-md-1">
                                        <a href="/product/{{$val->product->get(0)->id}}">
                                            <img class="img-fluid"
                                                src="{{ asset('storage/' . $val->product->get(0)->image)}}"
                                                alt="img-placeholder" />
                                        </a>
                                    </div>
                                    <div class="col-md-11">
                                        <p class="fw-bolder">{{$val->product->get(0)->name}}
                                            ({{$val->product->get(0)->size->name}})
                                            -
                                            {{$val->product->get(0)->color->name}}</p>
                                        <p class="fs-12">{{$val->quantity}} barang x Rp,{{$val->product->get(0)->price}}
                                        </p>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-2 mt-1">
                                <p class="text-mute">Total payment:</p>
                                <p class="fw-bolder">Rp,{{$cart->order->get(0)->gross_amount}}</p>
                                <form action="/order-details/{{$cart->order->get(0)->unique_code}}" method="POST">
                                    @csrf
                                    <a href="/order-details/{{$cart->unique_code}}"
                                        class="btn btn-sm btn-info">details</a>
                                    @if ($cart->order->get(0)->status == 'in delivery')
                                    <input type="hidden" name="status" value="delivered">
                                    <button type="submit" class="btn btn-sm btn-warning" onclick="return confirm('Are you sure the product has arrived?')">Confirm</button>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <div class="text-center">

                    <div class="badge rounded-pill badge-light-primary">Order still empty, please checkout to see order
                        list.
                        <a class="text-success" href="/checkout"> Checkout now.</a>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
