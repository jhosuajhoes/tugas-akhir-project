@extends('partial.app')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Order Details</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active"><a href="/order">Order</a>
                                </li>
                                <li class="breadcrumb-item active">Details
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="invoice-preview-wrapper">
                <div class="row invoice-preview">
                    <!-- Invoice -->
                    <div class="col-md-12">
                        <div class="card invoice-preview-card">
                            <div class="card-body invoice-padding pb-0">
                                <!-- Header starts -->
                                <div
                                    class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                                    <div>
                                        <h3 class="text-primary invoice-logo">Skysea.co</h3>
                                        <p class="card-text mb-25">Pogung Lor Office, Sinduadi, Sleman</p>
                                        <p class="card-text mb-25">Yogyakarta, 55284, Indonesia</p>
                                        <p class="card-text mb-0">+62 822 541 682 12</p>
                                    </div>
                                    <div class="mt-md-0 mt-2">
                                        <h4 class="invoice-title">
                                            Invoice
                                            <span class="invoice-number">#{{$order->unique_code}}</span>
                                            <br>
                                            <a class="btn btn-sm btn-warning mt-1"
                                                href="/order-invoice-print/{{$order->unique_code}}"
                                                target="_blank">Download</a>
                                        </h4>
                                        <div>
                                        </div>
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title">Date Order:</p>
                                            <p class="invoice-date">{{$order->date}}</p>
                                        </div>
                                        @if ($order->status == 'settlement')
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title">Status</p>
                                            <p class="invoice-date">: <span class="badge badge-light-success">On
                                                    Process</span></p>
                                        </div>
                                        @elseif ($order->status == 'capture')
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title">Status</p>
                                            <p class="invoice-date">: <span class="badge badge-light-success">On
                                                    Process</span></p>
                                        </div>
                                        @elseif ($order->status == 'pending')
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title">Status</p>
                                            <p class="invoice-date">: <span class="badge badge-light-warning">Waiting
                                                    payment</span></p>
                                        </div>
                                            <div class="text-end">
                                                @if ($order->payment->payment_type == 'cstore')
                                                <a class="btn btn-sm btn-secondary mt-1 w-100"
                                                href="{{$order->payment->invoice}}"
                                                target="_blank">How to pay?</a>
                                                @else
                                                @endif
                                            </div>
                                        @elseif ($order->status == 'in delivery')
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title">Status</p>
                                            <p class="invoice-date">: <span class="badge badge-light-success">on
                                                    delivery</span></p>
                                        </div>
                                        @elseif ($order->status == 'delivered')
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title">Status</p>
                                            <p class="invoice-date">: <span class="badge badge-light-success">Order
                                                    Complete</span></p>
                                        </div>
                                        @else
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title">Status</p>
                                            <p class="invoice-date">: <span
                                                    class="badge badge-light-danger">Cancel</span></p>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <!-- Header ends -->
                            </div>

                            <hr class="invoice-spacing" />

                            <!-- Address and Contact starts -->
                            <div class="card-body invoice-padding pt-0">
                                <div class="row invoice-spacing">
                                    <div class="col-xl-5 p-0 mt-xl-0 mt-2">
                                        <h6 class="mb-2">Shipping Details:</h6>
                                        <table class="fs-14">
                                            <tbody>
                                                <tr>
                                                    <td class="pe-1">Courier</td>
                                                    <td><span class="text-uppercase">:
                                                            {{$order->shipment->courier}}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="pe-1">Address</td>
                                                    <td>: {{$order->shipment->address}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="pe-1"></td>
                                                    <td>{{$order->shipment->city->city_name}},
                                                        {{$order->shipment->city->province->province}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xl-5 p-0 mt-xl-0 mt-2">
                                        <h6 class="mb-2">Payment Details:</h6>
                                        <table class="fs-14">
                                            <tbody>
                                                <tr>
                                                    <td class="pe-1">Total Due</td>
                                                    <td><span class="fw-bold">: {{$order->payment->gross_amount}}</span>
                                                    </td>
                                                </tr>
                                                @if ($order->payment->payment_type == 'cstore')
                                                    <tr>
                                                        <td class="pe-1">Payment Code</td>
                                                        <td class="text-uppercase">: {{$order->payment->payment_code}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="pe-1">Payment type</td>
                                                        <td class="text-uppercase">: {{$order->payment->payment_type}}</td>
                                                    </tr>
                                                @elseif ($order->payment->payment_type == 'bank_transfer')
                                                    @if ($order->status == 'pending')
                                                    <tr>
                                                        <td class="pe-1"><span class="text-warning"><strong>Please Transfer to</strong></span></td>
                                                        <td class="text-uppercase"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="pe-1"><span>Virtual Account No. </span></td>
                                                        <td class="text-uppercase fw-bolder">: {{$order->payment->va_number}} ({{$order->payment->bank}})</td>
                                                    </tr>
                                                    @else
                                                    <tr>
                                                        <td class="pe-1">Payment type</td>
                                                        <td class="text-uppercase">: {{$order->payment->payment_type}}
                                                            ({{$order->payment->bank}})</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="pe-1">No.Virtual Account</td>
                                                        <td class="text-uppercase">: {{$order->payment->va_number}}</td>
                                                    </tr>
                                                    @endif
                                                @else
                                                <tr>
                                                    <td class="pe-1">Payment type</td>
                                                    <td class="text-uppercase">: {{$order->payment->payment_type}}</td>
                                                </tr> @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xl-2 p-0">
                                        <h6 class="mb-2">Invoice To:</h6>
                                        <h6 class="mb-25">{{$order->cart->user->name}}</h6>
                                        <p class="card-text mb-25">{{$order->cart->user->phone_number}}</p>
                                        <p class="card-text mb-0">{{$order->cart->user->email}}</p>
                                    </div>

                                </div>
                            </div>
                            <!-- Address and Contact ends -->

                            <!-- Invoice Description starts -->
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="py-1">Product description</th>
                                            <th class="py-1">Color</th>
                                            <th class="py-1">Size</th>
                                            <th class="py-1">Qty</th>
                                            <th class="py-1">Price</th>
                                            <th class="py-1">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($order_cart as $result)
                                        <tr class="border-bottom">
                                            <td class="py-1">
                                                <p class="card-text fw-bold mb-25">{{$result->product->get(0)->name}}
                                                </p>
                                            </td>
                                            <td class="py-1">
                                                <span class="fw-bold">{{$result->product->get(0)->color->name}}</span>
                                            </td>
                                            <td class="py-1">
                                                <span class="fw-bold">{{$result->product->get(0)->size->name}}</span>
                                            </td>
                                            <td class="py-1">
                                                <span class="fw-bold">{{$result->quantity}}</span>
                                            </td>
                                            <td class="py-1">
                                                <span class="fw-bold">{{$result->product->get(0)->price}}</span>
                                            </td>
                                            <td class="py-1">
                                                <span
                                                    class="fw-bold">{{$result->quantity * $result->product->get(0)->price}}</span>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="card-body invoice-padding pb-0">
                                <div class="row invoice-sales-total-wrapper">
                                    <div class="col-md-6 order-md-1 order-2 mt-md-0 mt-3">
                                    </div>
                                    <div class="col-md-6 d-flex justify-content-end order-md-2 order-1">
                                        <div class="invoice-total-wrapper">
                                            <div class="invoice-total-item">
                                                <p class="invoice-total-title">Courier:</p>
                                                <p class="invoice-total-amount text-uppercase">
                                                    {{$order->shipment->courier}}</p>
                                            </div>
                                            <div class="invoice-total-item">
                                                <p class="invoice-total-title">Shipping cost:</p>
                                                <p class="invoice-total-title text-warning">
                                                    {{$order->shipment->shipping_cost}}</p>
                                            </div>
                                            <hr class="my-50" />
                                            <div class="invoice-total-item">
                                                <p class="invoice-total-title">Total:</p>
                                                <p class="invoice-total-amount">{{$order->payment->gross_amount}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Invoice Description ends -->

                            <hr class="invoice-spacing" />

                            <!-- Invoice Note starts -->
                            <div class="card-body invoice-padding pt-0">
                                <div class="row">
                                    <div class="col-12">
                                        <span class="fw-bold">Note:</span>
                                        <span>Thank you for purchasing our products, we hope that the products we provide can provide comfort to our customers.
                                            <br>
                                            *Please keep this invoice properly.</span>
                                    </div>
                                </div>
                            </div>
                            <!-- Invoice Note ends -->
                        </div>
                    </div>
                    <!-- /Invoice -->

                    {{-- <!-- Invoice Actions -->
                    <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
                        <div class="card">
                            <div class="card-body">
                                @if ($order->status == 'settlement')
                                <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                    @csrf
                    <input type="hidden" name="status" value="in delivery">
                    <button type="submit" class="btn btn-primary w-100 mb-75">
                        Set to Delivery
                    </button>
                    </form>
                    @elseif ($order->status == 'in delivery')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        <input type="hidden" name="status" value="delivered">
                        <button type="submit" class="btn btn-secondary w-100 mb-75">
                            Set to Done
                        </button>
                    </form>
                    @elseif ($order->status == 'pending')
                    <form action="/admin/order-details/{{$order->unique_code}}" method="POST">
                        @csrf
                        <input type="hidden" name="status" value="canceled">
                        <button type="submit" class="btn btn-danger w-100 mb-75">
                            Cancel order
                        </button>
                    </form>
                    @elseif ($order->status == 'delivered')
                    <span class="badge rounded-pill badge-light-success me-1 mt-1 mb-2">Order has been completed</span>
                    @else
                    <a href="#" class="btn btn-warning w-100 btn-download-invoice mb-75">Order error!</a>
                    @endif
                    <a class="btn btn-outline-secondary w-100 mb-75" href="/admin/order-invoice/{{$order->unique_code}}"
                        target="_blank">
                        Print </a>
                </div>
        </div>
    </div>
    <!-- /Invoice Actions --> --}}
</div>
</section>
</div>
</div>
</div>

@endsection
