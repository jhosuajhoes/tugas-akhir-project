@extends('partial.loginApp')

@section('content')

<!-- BEGIN: Content-->

<div class="content-overlay"></div>
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <div class="auth-wrapper auth-basic px-2">
            <div class="auth-inner my-2">
                {{-- @if(session()->has('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif --}}
            @if(session()->has('fail'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('fail') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            <!-- Register basic -->
            <div class="card mb-0">
                <div class="card-body">
                    <a href="index.html" class="brand-logo">
                        <h2 class="brand-text text-primary ms-1">Skysea.co</h2>
                    </a>
                    <h4 class="card-title mb-1">Welcome in Skysea.co 🚀</h4>
                    <p class="card-text mb-2">Please register and create your account to start shopping!</p>
                    <form  class="auth-login-form mt-2" action="/register" method="POST">
                        @csrf
                        <div class="mb-1">
                            <label for="register-username" class="form-label">Username</label>
                            <input type="text" id="name_field" class="form-control @error('name') is-invalid @enderror"
                                id="register-username" pattern="^[A-Za-z -]+$" name="name" placeholder="username"
                                aria-describedby="register-username" tabindex="1" value="{{old('name')}}" autofocus
                                required />
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="mb-1">
                            <label for="register-username" class="form-label">Phone Number</label>
                            <input type="number" class="form-control @error('phone_number') is-invalid @enderror"
                                id="phone_number" name="phone_number" placeholder="Phone Number" tabindex="1"
                                value="{{old('phone_number')}}" autofocus required />
                            @error('phone_number')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="mb-1">
                            <label for="register-email" class="form-label">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                id="register-email" name="email" placeholder="user@email.com"
                                aria-describedby="register-email" tabindex="2" value="{{old('email')}}" required />
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="mb-1">
                            <label for="register-email" class="form-label">Address</label>
                            <textarea class="form-control @error('address') is-invalid @enderror" name="address"
                                id="exampleFormControlTextarea1" rows="1"
                                placeholder="Home number, Street name, Apartment, etc">{{old('address')}}</textarea>
                            @error('address')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="mb-1">
                            <label for="register-password" class="form-label">Password</label>
                            <div class="input-group input-group-merge form-password-toggle">
                                <input type="password"
                                    class="form-control form-control-merge @error('password') is-invalid @enderror"
                                    id="register-password" name="password"
                                    placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                    aria-describedby="register-password" tabindex="3" required />
                                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="mt-1 fs-12">
                                <p class="fw-bolder">Password requirements:</p>
                                <ul class="ps-1 ms-25">
                                    <li class="mb-50">Minimum 8 characters long - the more, the better</li>
                                    <li class="mb-50">At least one lowercase and uppercase character</li>
                                    <li>At least one number</li>
                                </ul>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary w-100" tabindex="5">Sign up</button>
                    </form>

                    <p class="text-center mt-2">
                        <span>Already have an account?</span>
                        <a href="/login">
                            <span>Sign in instead</span>
                        </a>
                    </p>

                    <div class="divider my-2">
                        <div class="divider-text">or</div>
                    </div>
                        <a href="{{route('google.login')}}" class="btn btn-md btn-outline-secondary w-100">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-google" viewBox="0 0 16 16">
                                <path
                                    d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301 1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z" />
                            </svg>
                            Sign in with Google
                        </a>
                </div>
            </div>
            <!-- /Register basic -->
        </div>
    </div>

</div>
</div>

@endsection
<!-- END: Content-->
