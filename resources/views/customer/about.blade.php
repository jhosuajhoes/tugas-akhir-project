@extends('partial.app')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">About</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active">About us
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row col-12 p-2">
                <div class="col-lg-12 col-md-12">

                </div>
                <div class="card p-4">
                    <h1 class="text-center text-primary"><strong>Skysea.co</strong> </h1>
                    <div class="card-body fs-18">
                        <p class="text-center" style="line-height: 30px"><strong>Skysea.co adalah sebuah online shop yang mulai berdiri pada
                                tanggal 19 november 2019. </strong>
                            Skysea.co itu sendiri diambil dari kata sky yg artinya langit dimana dalam artian kita
                            sebagai manusia harus bisa bermimpi setinggi tingginya sama dengan halnya clothing ini
                            sangat amat mempunyai cita - cita yang tinggi dan sea yang berarti lautan dimana artian
                            kita sebagai manusia saat bermimpi dan menjalani hari-harinya harus mempunyai rasa tabah
                            dan sabar seluas lautan karena semua butuh proses. Kami menjual produk fashion pria
                            maupun wanita berupa kaos dan celana.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
