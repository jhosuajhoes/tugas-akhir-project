@extends('partial.app')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Account</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/profile">Account Settings </a>
                                </li>
                                <li class="breadcrumb-item active"> Account
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills mb-2">
                        <!-- account -->
                        <li class="nav-item">
                            <a class="nav-link active" href="page-account-settings-account.html">
                                <i data-feather="user" class="font-medium-3 me-50"></i>
                                <span class="fw-bold">Account</span>
                            </a>
                        </li>
                        <!-- security -->
                        <li class="nav-item">
                            <a class="nav-link" href="/profile-security">
                                <i data-feather="lock" class="font-medium-3 me-50"></i>
                                <span class="fw-bold">Security</span>
                            </a>
                        </li>
                    </ul>

                    <!-- profile -->
                    <div class="card">
                        <div class="card-header border-bottom">
                            <h4 class="card-title">Profile Details</h4>
                        </div>
                        <div class="card-body py-2 my-25">
                            <!-- form -->
                            <form action="/profile/{{auth()->user()->id}}" method="POST">
                                @csrf
                                <div class="">
                                    <div class="col-12 col-sm-6 mb-1">
                                        <label class="form-label" for="name">Username</label>
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                            id="name" name="name" value="{{auth()->user()->name}}" />
                                        @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    {{-- <div class="col-12 col-sm-6 mb-1">
                                        <label class="form-label" for="accountEmail">Email</label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                            id="accountEmail" name="email" placeholder="Email"
                                            value="{{auth()->user()->email}}" disabled/>
                                        @error('email')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div> --}}
                                    <div class="col-12 col-sm-6 mb-1">
                                        <label class="form-label" for="accountPhoneNumber">Phone Number</label>
                                        <input type="text"
                                            class="form-control account-number-mask @error('phone_number') is-invalid @enderror"
                                            id="accountPhoneNumber" name="phone_number" placeholder="Phone Number"
                                            value="{{auth()->user()->phone_number}}" />
                                        @error('phone_number')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-12 col-sm-6 mb-1">
                                        <label class="form-label" for="accountAddress">Address</label>
                                        <textarea class="form-control @error('address') is-invalid @enderror"
                                            name="address" id="address" rows="3"
                                            placeholder="Home number, Street name, Apartment, etc">{{auth()->user()->address}}</textarea>
                                        @error('address')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror

                                    </div>
                                    <button type="submit" class="btn btn-primary mt-1 me-1">Save changes</button>
                                    <button type="reset" class="btn btn-outline-secondary mt-1">Discard</button>
                                </div>
                        </div>
                        </form>
                        <!--/ form -->
                    </div>
                </div>

                <div class="col-12">
                    <!-- deactivate account  -->
                    <div class="card">
                        <div class="card-header border-bottom">
                            <h4 class="card-title">Delete Account</h4>
                        </div>
                        <div class="card-body py-2 my-25">
                            <div class="alert alert-warning">
                                <h4 class="alert-heading">Are you sure you want to delete your account?</h4>
                                <div class="alert-body fw-normal">
                                    Once you delete your account, there is no going back. Please be certain.
                                </div>
                            </div>

                            <form action="/profile-delete/{{auth()->user()->id}}" method="post">
                                @method('delete')
                                @csrf
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="checkbox"
                                        id="accountActivation" value="1" data-msg="Please confirm you want to delete account" required/>
                                    <label class="form-check-label font-small-3" for="accountActivation">
                                        I confirm my account deactivation
                                    </label>
                                </div>
                                <div>
                                        <button type="submit" class="btn btn-danger deactivate-account mt-1">Deactivate
                                            Account</button>
                                    </form>
                                </div>
                        </div>
                    </div>
                    <!--/ profile -->
                </div>
            </div>
        </div>

    </div>
</div>
</div>
<!-- END: Content-->

@endsection
