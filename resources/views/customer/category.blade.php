@extends('partial.app')

@section('content')

<!-- BEGIN: Content-->
<div class="row">
    <div class="col-12">
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-body">
                    <!-- background Overlay when sidebar is shown  starts-->
                    <div class="body-content-overlay"></div>
                    <!-- background Overlay when sidebar is shown  ends-->
                    <div class="content-header row">
                        <div class="content-header-left col-md-9 col-12">
                            <div class="row breadcrumbs-top">
                                <div class="col-12">
                                    <h2 class="content-header-title float-start mb-0">Shop</h2>
                                    <div class="breadcrumb-wrapper">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/">Home</a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="/shop">Shop</a>
                                            </li>
                                            <li class="breadcrumb-item active">Products
                                            </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- E-commerce Search Bar Starts -->
                    <div class="container-sm search-results text-right">{{$search_results}} results found</div>
                    <section id="ecommerce-searchbar" class="ecommerce-searchbar container-sm">
                        <form action="/shop" method="GET">
                            <div class="row mt-1">
                                <div class="col-sm-12">
                                    <div class="input-group input-group-merge">
                                        <input type="text" class="form-control search-product" id="shop-search"
                                            placeholder="Search Product" name="search" value="{{request('search')}}" />
                                        <span class="input-group-text"><button class="btn btn-outline-none"
                                                id="button-addon2" type="submit"><i data-feather="search"
                                                    class="text-muted"></i></button></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </section>
                    <!-- E-commerce Search Bar Ends -->

                    <!-- E-commerce Products Starts -->
                    <section id="ecommerce-products" class="container-sm grid-view">
                        @foreach ($products as $product)
                        <div class="card ecommerce-card">
                            <div class="item-img text-center">
                                @if ($product->image)
                                <a href="/product/{{$product->id}}">
                                    <img class="card-img-top" src="{{ asset('storage/' . $product->image)}}"
                                        alt="img-placeholder" /></a>
                                @else
                                <a href="">
                                    <img class="img-fluid card-img-top"
                                        src="https://source.unsplash.com/user/erondu/315x265"
                                        alt="img-placeholder" /></a>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="item-wrapper">
                                    <div>
                                        <h6 class="item-price">Rp{{$product->price}}</h6>
                                    </div>
                                </div>
                                <h6 class="item-name">
                                    <a class="text-body" href="app-ecommerce-details.html">{{$product->name}}</a>
                                </h6>
                                <p class="card-text item-description">
                                    {{$product->description}}
                                </p>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price">{{$product->price}}</h4>
                                    </div>
                                </div>

                                <form action="/product/wishlist" method="post">
                                    @csrf
                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                    <button type="submit" class="btn btn-secondary m-l-10">
                                        <i data-feather="heart"></i>
                                        <span>Wishlist</span>
                                    </button>
                                <a href="/product/{{$product->id}}" class="btn btn-primary btn-block">
                                    <i data-feather="shopping-cart"></i>
                                    <span class="add-to-cart">See details</span>
                                </a>
                                </form>
                            </div>
                        </div>
                        @endforeach
                    </section>
                    <!-- E-commerce Products Ends -->
                    <!-- E-commerce Pagination Starts -->
                    <section id="ecommerce-pagination">
                        <div class="row">
                            <div class="col-sm-12 justify-content-center">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center mt-2">
                                        {{$products->links()}}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </section>
                    <!-- E-commerce Pagination Ends -->

                </div>
                {{-- <div class="sidebar-detached sidebar-left">
                    <div class="sidebar">
                        <!-- Ecommerce Sidebar Starts -->
                        <div class="sidebar-shop">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h6 class="filter-heading d-none d-lg-block">Filters</h6>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <!-- Price Filter starts -->
                                    <form action="/shop" method="GET">
                                        <div class="multi-range-price">
                                            <h6 class="filter-title mt-0">Multi Range</h6>
                                            <ul class="list-unstyled price-range" id="price-range">
                                                <li>
                                                    <div class="form-check">
                                                        <input type="radio" id="priceAll" name="price-range" value="0"
                                                            class="form-check-input" />
                                                        <label class="form-check-label" for="priceAll">All</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="form-check">
                                                        <input type="radio" id="priceRange1" name="price-range"
                                                            class="form-check-input" value="50000" {{ old('price-range') == "50000" ? 'checked' : '' }}/>
                <label class="form-check-label" for="priceRange1">&lt;
                    Rp50000</label>
            </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="radio" id="priceRange2" name="price-range" class="form-check-input"
                        value="50000-75000" />
                    <label class="form-check-label" for="priceRange2">Rp50000 -
                        Rp75000</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="radio" id="priceARange3" name="price-range" class="form-check-input"
                        value="75000-120000" />
                    <label class="form-check-label" for="priceARange3">Rp75000 -
                        Rp120000</label>
                </div>
            </li>
            <li>
                <div class="form-check">
                    <input type="radio" id="priceRange4" name="price-range" class="form-check-input" value="120000" />
                    <label class="form-check-label" for="priceRange4">&gt;
                        Rp120000</label>
                </div>
            </li>
            </ul>
        </div>
        <!-- Price Filter ends -->
        <!-- Categories Starts -->
        <div id="product-categories">
            <h6 class="filter-title">Categories</h6>
            <ul class="list-unstyled categories-list">
                @foreach ($categories as $category )
                <li>
                    <div class="form-check">
                        <input type="radio" id="category1" name="category-filter" class="form-check-input"
                            value="{{$category->name}}" />
                        <label class="form-check-label" for="category">{{$category->name}}</label>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
        <!-- Categories Ends -->
        <!-- Clear Filters Starts -->
        <div id="clear-filters">
            <button type="submit" class="btn btn-sm btn-warning me-1">Filter</button>
            <a href="/shop" class="btn btn-sm btn-outline-info">Reset</a>
        </div>
        <!-- Clear Filters Ends -->
        </form>
    </div>

</div>
</div>
<!-- Ecommerce Sidebar Ends -->

</div>
</div> --}}
</div>
</div>
</div>
</div>


<!-- END: Content-->

@endsection
