{{-- @dd($checkouts) --}}

{{-- @dd($checkouts) --}}
@extends('partial.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="app-content content ecommerce-application">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper container-xxl p-0">
                <div class="content-header row">
                    <div class="content-header-left col-md-9 col-12 mb-2">
                        <div class="row breadcrumbs-top">
                            <div class="col-12">
                                <h2 class="content-header-title float-start mb-0">Checkout</h2>
                                <div class="breadcrumb-wrapper">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html">Home</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#">eCommerce</a>
                                        </li>
                                        <li class="breadcrumb-item active">Checkout
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                        <div class="mb-1 breadcrumb-right">
                            <div class="dropdown">
                                <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                                        data-feather="grid"></i></button>
                                <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item"
                                        href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span
                                            class="align-middle">Todo</span></a><a class="dropdown-item"
                                        href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span
                                            class="align-middle">Chat</span></a><a class="dropdown-item"
                                        href="app-email.html"><i class="me-1" data-feather="mail"></i><span
                                            class="align-middle">Email</span></a><a class="dropdown-item"
                                        href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span
                                            class="align-middle">Calendar</span></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Checkout Place order starts -->
                <!-- Modal konfirmasi payments -->
                {{-- <div class="modal fade text-start modal-warning" id="warning" tabindex="-1"
                    aria-labelledby="myModalLabel140" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel140">Payment</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                Pastikan alamat tujuan pengiriman yang anda isi sudah benar.
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="pay-button" class="btn btn-warning"
                                    data-bs-dismiss="modal">Pay</button>
                            </div>
                        </div>
                    </div>
                </div> --}}

                <div class="content">
                    <form class="auth-login-form" action="/payment" method="POST">
                        @csrf
                        <div id="place-order" class="list-view product-checkout">
                            <div class="checkout-items">
                                <div class="card p-1">
                                    <div class="card-header">
                                        <h4 class="card-title">Customer Details</h4>
                                    </div>
                                    <div class="card-body actions">
                                        <div class="row">
                                            <div class="col-md-10 col-12 mb-1 position-relative">
                                                <label class="form-label mb-1" for="validationTooltip01">Full name</label>
                                                <input type="text" id="name_field" class="form-control" name="name"
                                                    value="{{auth()->user()->name}}" id="name"
                                                    placeholder="First name" pattern="/^[a-zA-Z\s]*$/g" required />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10 col-12 mb-1 position-relative">
                                                <label class="form-label mb-1" for="validationTooltip02">Email</label>
                                                <input type="email" class="form-control" name="email"
                                                    value="{{auth()->user()->email}}" id="email"
                                                    placeholder="Email" required />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10 col-12 mb-1 position-relative">
                                                <label class="form-label mb-1" for="validationTooltip01">Phone Number</label>
                                                <input type="number" class="form-control" name="phone_number"
                                                    value="{{auth()->user()->phone_number}}" id="phone_number"
                                                    placeholder="Phone Number" required />
                                            </div>
                                        </div>
                                        <div class="row g-1">
                                            <div class="col-md-5 col-12 mb-1 position-relative">
                                                <label class="form-label mb-1" for="select2-basic">Province</label>
                                                <select class="form-select" name="province_destination"
                                                    id="province_destination" required>
                                                    <option value="" holder>- Select Province -</option>
                                                    @foreach ($provinces as $result )
                                                    <option value="{{$result->id}}">{{$result->province}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-5 col-12 mb-1 position-relative">
                                                <label class="form-label mb-1" for="basicSelect">City</label>
                                                <select id="destination" name="destination" class="form-select"
                                                    required>
                                                    <option>- Select City -</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-10">
                                            <div class="mb-1">
                                                <label class="form-label mb-1"
                                                    for="exampleFormControlTextarea1">Address details</label>
                                                <textarea class="form-control"
                                                name="address" id="exampleFormControlTextarea1" rows="3"
                                                    placeholder="Home number, Street name, Apartment, etc">{{auth()->user()->address}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="checkout-options">
                                <div class="customer-card">
                                    <div class="card p-1">
                                        <div class="card-header">
                                            <h4 class="card-title">Order Details</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive fs-8">
                                                <table class="table" style="font-size: 13px">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Qty</th>
                                                            <th>Subtotal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                        $total = 0 ;
                                                        //hitung berat
                                                        $berat = $weight*150;
                                                        @endphp
                                                        @foreach ($checkouts as $checkout)
                                                        <tr>
                                                            <td>{{$checkout->product->get(0)->name}} -
                                                                {{$checkout->product->get(0)->color->name}},{{$checkout->product->get(0)->size->name}}
                                                            </td>
                                                            <td>{{$checkout->quantity}}</td>
                                                            <td>Rp{{ $checkout->quantity * $checkout->product->get(0)->price }}
                                                            </td>
                                                        </tr>
                                                        @php
                                                        $total += $checkout->product->get(0)->price *
                                                        $checkout->quantity
                                                        @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr />
                                            <div class="mb-1">
                                                <h6 class="price-title">Shipping Delivery</h6>
                                                <input type="hidden" name="weight" id="weight" value="{{$berat}}">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <select class="form-select" id="courier" name="courier" required>
                                                            <option value="" holder>- Select Courier -</option>
                                                            <option value="jne">JNE</option>
                                                            <option value="pos">POS</option>
                                                            <option value="tiki">TIKI</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div>
                                                    <select name="ongkir" id="ongkir" class="form-select mt-1 mb-1" required>
                                                        <option value="" holder>- Shipping Package -</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="price-details">
                                                <h6 class="price-title">Ringkasan Belanja</h6>
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title">Estimasi Berat</div>
                                                        <div class="detail-amt">{{$berat}}g</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Sub Total</div>
                                                        <input type="hidden" id="estimasi" name="estimasi" required>
                                                        <input type="hidden" id="sub_total" value="{{$total}}">
                                                        <div class="detail-amt">Rp{{$total}}</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Ongkir</div>
                                                        <input type="text" id="biaya_ongkir" name="ongkir"
                                                            class="text-right detail-amt discount-amt text-success"
                                                            placeholder="-" readonly="" required>
                                                    </li>
                                                </ul>
                                                <hr />
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title detail-total">Harga Total</div>

                                                        <input name="total_bayar" type="text" id="total"
                                                            class="text-right detail-amt bolder" placeholder="-"
                                                            readonly>
                                                    </li>
                                                </ul>
                                                <input type="hidden" id="order_id" name="order_id" value="{{$checkout->unique_code}}">
                                                <button type="submit" class="btn btn-primary w-100 btn-next">
                                                    Continue to Pay
                                                </button>

                    </form>
                    {{-- <button class="btn btn-primary w-100 btn-next">Pay!</button> --}}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" language="JavaScript">
    $(document).on('keypress', '#name_field', function (event) {
        var regex = new RegExp("^[a-zA-Z ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        /* ajax mendapatkan data kota */
        $(function () {
            $('#province_destination').on('change', function () {
                let province_id = $('#province_destination').val();
                $.ajax({
                    url: "{{route('getCities')}}",
                    type: "POST",
                    data: {
                        province_id: province_id
                    },
                    cache: false,
                    success: function (msg) {
                        $('#destination').html(msg);
                    },
                    error: function (data) {
                        console.log('error:', data)
                    },
                });
            });
        });
        /* ajax mendapatkan data ongkir */
        $(function () {
            $('#courier').on('change', function () {
                let destination = $('#destination').val();
                let weight = $('#weight').val();
                let courier = $('#courier').val();
                $.ajax({
                    url: "{{route('getCourier')}}",
                    type: "POST",
                    data: {
                        destination: destination,
                        weight: weight,
                        courier: courier
                    },
                    cache: false,
                    success: function (msg) {
                        $('#ongkir').html(msg);
                    },
                    error: function (data) {
                        console.log('error:', data)
                    },
                });
            });
        });
        /* mengambil cost ongkir */
        $(function () {
            $('#ongkir').on('change', function () {
                var ongkir = $('#ongkir').val();
                var biaya = parseInt(ongkir);
                $('#biaya_ongkir').val(biaya);
            });
        });
        /* mengambil estimasi pengiriman */
        $(function () {
            $('#ongkir').on('change', function () {
                var estimasi = $('#ongkir').val();
                var est = estimasi.split("|");
                var time = est[1];
                $('#estimasi').val(time[0]);
            });
        });
        /* hitung total bayar otomatis */
        $(function () {
            $('#ongkir').on('change', function () {
                var ongkir = $('#ongkir').val();
                var subTotal = $('#sub_total').val();

                var total = parseInt(ongkir) + parseInt(subTotal);
                $('#total').val(total);
            });
        });
    })

</script>

@endsection
