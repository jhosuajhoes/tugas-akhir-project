<!-- BEGIN: Navbar-->
<nav class="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
    data-nav="brand-center">
    <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav">
            <li class="nav-item"><a class="navbar-brand" href="/"><span class="brand-logo">
                        <h1 class="brand-text mb-0">Skysea.co</h1>
                </a></li>
        </ul>
    </div>
    <div class="navbar-container d-flex content">
        <div class="bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav d-xl-none">
                <li class="nav-item"><a class="nav-link menu-toggle" href="#"><i class="ficon"
                            data-feather="menu"></i></a></li>
            </ul>
            <ul class="nav navbar-nav bookmark-icons">
                <li class="nav-item d-none d-lg-block"><a class="nav-link" href="mailto:https://shutx2306@gmail.com"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Email"><i class="ficon"
                            data-feather="mail"></i></a></li>
                <li class="nav-item d-none d-lg-block"><a class="nav-link" href="https://wa.me/6282254168212"
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Chat"><i class="ficon"
                            data-feather="message-square"></i></a>
                </li>
            </ul>
        </div>
        <ul class="nav navbar-nav align-items-center ms-auto">
            @auth
            <li class="nav-item me-25"><a class="nav-link" href="/order"><i
                class="ficon" data-feather="bell"></i><span
                class="badge rounded-pill bg-warning badge-up cart-item-count">{{$count_order}}</span></a>
            </li>
            <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                        class="ficon" data-feather="shopping-cart"></i><span
                        class="badge rounded-pill bg-primary badge-up cart-item-count">{{$count}}</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 me-auto">My Cart</h4>
                            <div class="badge rounded-pill badge-light-primary">{{$count}} Items</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        @foreach ($checkouts as $checkout )
                        <div class="list-item align-items-center"><img class="d-block rounded me-1"
                                src="{{ asset('storage/' . $checkout->product->get(0)->image)}}" alt="donuts" width="62">
                            <div class="list-item-body flex-grow-1">
                                <div class="media-heading">
                                    <h6 class="cart-item-title"><a class="text-body" href="/checkout">
                                            {{$checkout->product->get(0)->name}}</a></h6><small
                                        class="cart-item">{{$checkout->quantity}} barang</small>
                                </div>
                                <h5 class="cart-item-price">Rp,{{$checkout->product->get(0)->price}}</h5>
                            </div>
                        </div>
                        @endforeach
                    </li>
                    <li class="text-center mt-1">
                        <a href="/checkout"><strong>
                                <p>See details</p>
                            </strong></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                        class="ficon" data-feather="heart"></i><span
                        class="badge rounded-pill bg-danger badge-up cart-item-count">{{$wish_count}}</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 me-auto">Wishlist</h4>
                            <div class="badge rounded-pill badge-light-danger">{{$wish_count}} Items</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        @foreach ($wishlist as $result )
                        <div class="list-item align-items-center"><img class="d-block rounded me-1"
                                src="{{ asset('storage/' . $result->product->get(0)->image)}}" alt="donuts" width="62">
                            <div class="list-item-body flex-grow-1">
                                <div class="media-heading">
                                    <h6 class="cart-item-title"><a class="text-body" href="/wishlist">
                                            {{$result->product->get(0)->name}}</a></h6><small
                                        class="cart-item">{{$result->quantity}} barang</small>
                                </div>
                                <h5 class="cart-item-price">Rp,{{$result->product->get(0)->price}}</h5>
                            </div>
                        </div>
                        @endforeach
                    </li>
                    <li class="text-center mt-1">
                        <a href="/wishlist"><strong>
                                <p>See details</p>
                            </strong></a>
                    </li>
                </ul>
            </li>

            @else

            <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                        class="ficon" data-feather="shopping-cart"></i><span
                        class="badge rounded-pill bg-primary badge-up cart-item-count">0</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex justify-content-center">
                            <div class="badge rounded-pill badge-light-primary">You're not loggin. Please login to see
                                the carts
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown dropdown-cart me-25"><a class="nav-link" href="#" data-bs-toggle="dropdown"><i
                        class="ficon" data-feather="heart"></i><span
                        class="badge rounded-pill bg-danger badge-up cart-item-count">0</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-end">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex justify-content-center">
                            <div class="badge rounded-pill badge-light-danger">You're not loggin. Please login to see
                                the wishlist
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            @endauth
            @auth
            <li class="nav-item dropdown dropdown-user">
                <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="user-nav d-sm-flex d-none"><span
                            class="user-name fw-bolder">{{ auth()->user()->name }}</span><span
                            class="user-status">{{ auth()->user()->roles }}</span>
                    </div>
                    <span class="avatar">
                        <div class="avatar bg-primary">
                            <div class="avatar-content">
                                <i data-feather="user" class="avatar-icon"></i>
                            </div>
                        </div>
                        <span class="avatar-status-online"></span>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                    <a class="dropdown-item" href="/profile"><i class="me-50" data-feather="user"></i>
                        Profile</a>
                    <div class="dropdown-divider"></div>
                    @can('admin')
                    <a class="dropdown-item" href="/admin/dashboard"><i class="me-50" data-feather="settings"></i> Admin
                        Panel</a>
                    @endcan
                    @can('owner')
                    <a class="dropdown-item" href="/admin/dashboard"><i class="me-50" data-feather="settings"></i> Admin
                        Panel</a>
                    @endcan
                    <form action="/logout" method="POST">
                        @csrf
                        <button type="submit" class="dropdown-item"><i class="me-50" data-feather="power"></i>
                            Logout</button>
                    </form>
                </div>
            </li>
            @else
            <li class="nav-item m-l-10">
                <a href="/login" class="btn btn-sm btn-primary">
                    Login
                </a>
            </li>
            @endauth
        </ul>
    </div>
</nav>
<!-- END: Navbar-->
