<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="description"
        content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Skysea</title>
    <link rel="apple-touch-icon" href="{{asset('themes/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('themes/app-assets/images/ico/favicon.ico')}}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
        rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/extensions/nouislider.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/vendors/css/charts/apexcharts.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/vendors/css/extensions/toastr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/vendors/css/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/forms/wizard/bs-stepper.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/themes/dark-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/themes/bordered-layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/themes/semi-dark-layout.css')}}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/css/core/menu/menu-types/horizontal-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/pages/dashboard-ecommerce.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/plugins/charts/chart-apex.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/css/plugins/extensions/ext-component-sliders.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/pages/app-ecommerce.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/css/plugins/extensions/ext-component-toastr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/pages/app-ecommerce-details.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/css/plugins/forms/form-number-input.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('themes/app-assets/css/plugins/forms/pickers/form-pickadate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/plugins/forms/form-wizard.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themesapp-assets/css/plugins/forms/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themesapp-assets/css/pages/authentication.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themesapp-assets/css/pages/modal-create-app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/pages/app-invoice.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/app-assets/css/pages/app-invoice-print.css')}}">



    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('themes/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/app-assets/css-rtl/custom-rtl.css')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">

    <!-- BEGIN: Custom CSS-->
    <style type="text/css">
        form input[type="text"]:focus {
            outline-color: none;
            outline-width: 0;
        }

        form input[readonly] {
            background-color: transparent;
            border: 0;
            font-size: 1em;
        }

    </style>
    <!-- END: Custom CSS-->

    <!-- BEGIN: Jquery-->
    <!-- @TODO: replace SET_YOUR_CLIENT_KEY_HERE with your client key -->
    <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="SB-Mid-client-BXv7A7DHjFC_WqkD"></script>
    <!-- Note: replace with src="https://app.midtrans.com/snap/snap.js" for Production environment -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('.movorder').trigger('click');
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
</head>
<!-- END: Head-->


<!-- BEGIN: Body-->

<body class="horizontal-layout horizontal-menu  navbar-floating footer-static  " data-open="hover"
    data-menu="horizontal-menu" data-col="">
    @include('sweetalert::alert')
    @include('partial.header')

    @include('partial.menubar')

    <!-- BEGIN: Content-->

    @yield('content')

    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    @include('partial.footer')


    <!-- BEGIN: Vendor JS-->
    <script src="{{asset('themes/app-assets/vendors/js/vendors.min.js')}}"></script>
    @yield('scripts')
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('themes/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/extensions/swiper.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/wizard/bs-stepper.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/cleave/cleave.min.js')}}"></script>
    <script src="{{asset('themes/app-assets/vendors/js/forms/cleave/addons/cleave-phone.us.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{asset('themes/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->
    <!-- choose one -->
    <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>
    <!-- BEGIN: Page JS-->
    <script src="{{asset('themes/app-assets/js/scripts/pages/app-ecommerce.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/forms/form-select2.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/forms/form-repeater.min.js') }}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/pages/app-ecommerce-details.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/forms/form-number-input.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/pages/app-ecommerce-checkout.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/pages/auth-login.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/pages/modal-add-new-address.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/pages/page-account-settings-account.js')}}"></script>
    <script src="{{asset('themes/app-assets/js/scripts/pages/app-invoice.js')}}"></script>

    <!-- END: Page JS-->
    <script src="https://code.iconify.design/2/2.0.3/iconify.min.js"></script>

    <script type="text/javascript">
        $(window).on('load', function () {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        });

        function hanyaAngka(event) {
            var angka = (event.which) ? event.which : event.keyCode
            if (angka != 46 && angka > 31 && (angka < 48 || angka > 57))
                return false;
            return true;
        }
    </script>
</body>
<!-- END: Body-->

</html>
